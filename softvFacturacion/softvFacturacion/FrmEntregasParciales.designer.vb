<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEntregasParciales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim FechaLabel As System.Windows.Forms.Label
        Dim CajeraLabel As System.Windows.Forms.Label
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim NumeroCepsaLabel As System.Windows.Forms.Label
        Dim RecibioLabel As System.Windows.Forms.Label
        Dim HoraLabel As System.Windows.Forms.Label
        Dim B1000Label As System.Windows.Forms.Label
        Dim B500Label As System.Windows.Forms.Label
        Dim B200Label As System.Windows.Forms.Label
        Dim B100Label As System.Windows.Forms.Label
        Dim B50Label As System.Windows.Forms.Label
        Dim B20Label As System.Windows.Forms.Label
        Dim M100Label As System.Windows.Forms.Label
        Dim M50Label As System.Windows.Forms.Label
        Dim M20Label As System.Windows.Forms.Label
        Dim M10Label As System.Windows.Forms.Label
        Dim M5Label As System.Windows.Forms.Label
        Dim M2Label As System.Windows.Forms.Label
        Dim M1Label As System.Windows.Forms.Label
        Dim M050Label As System.Windows.Forms.Label
        Dim M020Label As System.Windows.Forms.Label
        Dim M010Label As System.Windows.Forms.Label
        Dim M005Label As System.Windows.Forms.Label
        Dim ChequesLabel As System.Windows.Forms.Label
        Dim TarjetaLabel As System.Windows.Forms.Label
        Dim ReferenciaLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEntregasParciales))
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.CONPARCIALESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.FechaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ImporteTextBox = New System.Windows.Forms.TextBox()
        Me.NumeroCepsaTextBox = New System.Windows.Forms.TextBox()
        Me.RecibioTextBox = New System.Windows.Forms.TextBox()
        Me.HoraDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.B1000TextBox = New System.Windows.Forms.TextBox()
        Me.B500TextBox = New System.Windows.Forms.TextBox()
        Me.B200TextBox = New System.Windows.Forms.TextBox()
        Me.B100TextBox = New System.Windows.Forms.TextBox()
        Me.B50TextBox = New System.Windows.Forms.TextBox()
        Me.B20TextBox = New System.Windows.Forms.TextBox()
        Me.M100TextBox = New System.Windows.Forms.TextBox()
        Me.M50TextBox = New System.Windows.Forms.TextBox()
        Me.M20TextBox = New System.Windows.Forms.TextBox()
        Me.M10TextBox = New System.Windows.Forms.TextBox()
        Me.M5TextBox = New System.Windows.Forms.TextBox()
        Me.M2TextBox = New System.Windows.Forms.TextBox()
        Me.M1TextBox = New System.Windows.Forms.TextBox()
        Me.M050TextBox = New System.Windows.Forms.TextBox()
        Me.M020TextBox = New System.Windows.Forms.TextBox()
        Me.M010TextBox = New System.Windows.Forms.TextBox()
        Me.M005TextBox = New System.Windows.Forms.TextBox()
        Me.ChequesTextBox = New System.Windows.Forms.TextBox()
        Me.TarjetaTextBox = New System.Windows.Forms.TextBox()
        Me.ReferenciaTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRAUSUARIOS2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CONPARCIALESBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CONPARCIALESBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.CONPARCIALESTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.CONPARCIALESTableAdapter()
        Me.MUESTRAUSUARIOS2TableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRAUSUARIOS2TableAdapter()
        ConsecutivoLabel = New System.Windows.Forms.Label()
        FechaLabel = New System.Windows.Forms.Label()
        CajeraLabel = New System.Windows.Forms.Label()
        ImporteLabel = New System.Windows.Forms.Label()
        NumeroCepsaLabel = New System.Windows.Forms.Label()
        RecibioLabel = New System.Windows.Forms.Label()
        HoraLabel = New System.Windows.Forms.Label()
        B1000Label = New System.Windows.Forms.Label()
        B500Label = New System.Windows.Forms.Label()
        B200Label = New System.Windows.Forms.Label()
        B100Label = New System.Windows.Forms.Label()
        B50Label = New System.Windows.Forms.Label()
        B20Label = New System.Windows.Forms.Label()
        M100Label = New System.Windows.Forms.Label()
        M50Label = New System.Windows.Forms.Label()
        M20Label = New System.Windows.Forms.Label()
        M10Label = New System.Windows.Forms.Label()
        M5Label = New System.Windows.Forms.Label()
        M2Label = New System.Windows.Forms.Label()
        M1Label = New System.Windows.Forms.Label()
        M050Label = New System.Windows.Forms.Label()
        M020Label = New System.Windows.Forms.Label()
        M010Label = New System.Windows.Forms.Label()
        M005Label = New System.Windows.Forms.Label()
        ChequesLabel = New System.Windows.Forms.Label()
        TarjetaLabel = New System.Windows.Forms.Label()
        ReferenciaLabel = New System.Windows.Forms.Label()
        CType(Me.CONPARCIALESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.MUESTRAUSUARIOS2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONPARCIALESBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONPARCIALESBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConsecutivoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ConsecutivoLabel.Location = New System.Drawing.Point(28, 111)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(100, 15)
        ConsecutivoLabel.TabIndex = 2
        ConsecutivoLabel.Text = "Clave Entrega:"
        '
        'FechaLabel
        '
        FechaLabel.AutoSize = True
        FechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaLabel.Location = New System.Drawing.Point(48, 141)
        FechaLabel.Name = "FechaLabel"
        FechaLabel.Size = New System.Drawing.Size(50, 15)
        FechaLabel.TabIndex = 4
        FechaLabel.Text = "Fecha:"
        '
        'CajeraLabel
        '
        CajeraLabel.AutoSize = True
        CajeraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CajeraLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CajeraLabel.Location = New System.Drawing.Point(48, 176)
        CajeraLabel.Name = "CajeraLabel"
        CajeraLabel.Size = New System.Drawing.Size(71, 15)
        CajeraLabel.TabIndex = 6
        CajeraLabel.Text = "Cajero(a):"
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImporteLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ImporteLabel.Location = New System.Drawing.Point(204, 628)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(123, 15)
        ImporteLabel.TabIndex = 8
        ImporteLabel.Text = "Importe en Pesos:"
        '
        'NumeroCepsaLabel
        '
        NumeroCepsaLabel.AutoSize = True
        NumeroCepsaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeroCepsaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NumeroCepsaLabel.Location = New System.Drawing.Point(22, 205)
        NumeroCepsaLabel.Name = "NumeroCepsaLabel"
        NumeroCepsaLabel.Size = New System.Drawing.Size(106, 15)
        NumeroCepsaLabel.TabIndex = 10
        NumeroCepsaLabel.Text = "Numero Cepsa:"
        '
        'RecibioLabel
        '
        RecibioLabel.AutoSize = True
        RecibioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        RecibioLabel.ForeColor = System.Drawing.Color.LightSlateGray
        RecibioLabel.Location = New System.Drawing.Point(378, 141)
        RecibioLabel.Name = "RecibioLabel"
        RecibioLabel.Size = New System.Drawing.Size(60, 15)
        RecibioLabel.TabIndex = 12
        RecibioLabel.Text = "Recibio:"
        '
        'HoraLabel
        '
        HoraLabel.AutoSize = True
        HoraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HoraLabel.ForeColor = System.Drawing.Color.LightSlateGray
        HoraLabel.Location = New System.Drawing.Point(693, 132)
        HoraLabel.Name = "HoraLabel"
        HoraLabel.Size = New System.Drawing.Size(42, 15)
        HoraLabel.TabIndex = 14
        HoraLabel.Text = "Hora:"
        '
        'B1000Label
        '
        B1000Label.AutoSize = True
        B1000Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B1000Label.ForeColor = System.Drawing.Color.LightSlateGray
        B1000Label.Location = New System.Drawing.Point(204, 314)
        B1000Label.Name = "B1000Label"
        B1000Label.Size = New System.Drawing.Size(52, 15)
        B1000Label.TabIndex = 16
        B1000Label.Text = "B1000:"
        '
        'B500Label
        '
        B500Label.AutoSize = True
        B500Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B500Label.ForeColor = System.Drawing.Color.LightSlateGray
        B500Label.Location = New System.Drawing.Point(204, 340)
        B500Label.Name = "B500Label"
        B500Label.Size = New System.Drawing.Size(44, 15)
        B500Label.TabIndex = 18
        B500Label.Text = "B500:"
        '
        'B200Label
        '
        B200Label.AutoSize = True
        B200Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B200Label.ForeColor = System.Drawing.Color.LightSlateGray
        B200Label.Location = New System.Drawing.Point(204, 366)
        B200Label.Name = "B200Label"
        B200Label.Size = New System.Drawing.Size(44, 15)
        B200Label.TabIndex = 20
        B200Label.Text = "B200:"
        '
        'B100Label
        '
        B100Label.AutoSize = True
        B100Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B100Label.ForeColor = System.Drawing.Color.LightSlateGray
        B100Label.Location = New System.Drawing.Point(204, 392)
        B100Label.Name = "B100Label"
        B100Label.Size = New System.Drawing.Size(44, 15)
        B100Label.TabIndex = 22
        B100Label.Text = "B100:"
        '
        'B50Label
        '
        B50Label.AutoSize = True
        B50Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B50Label.ForeColor = System.Drawing.Color.LightSlateGray
        B50Label.Location = New System.Drawing.Point(204, 418)
        B50Label.Name = "B50Label"
        B50Label.Size = New System.Drawing.Size(36, 15)
        B50Label.TabIndex = 24
        B50Label.Text = "B50:"
        '
        'B20Label
        '
        B20Label.AutoSize = True
        B20Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B20Label.ForeColor = System.Drawing.Color.LightSlateGray
        B20Label.Location = New System.Drawing.Point(204, 444)
        B20Label.Name = "B20Label"
        B20Label.Size = New System.Drawing.Size(36, 15)
        B20Label.TabIndex = 26
        B20Label.Text = "B20:"
        '
        'M100Label
        '
        M100Label.AutoSize = True
        M100Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M100Label.ForeColor = System.Drawing.Color.LightSlateGray
        M100Label.Location = New System.Drawing.Point(669, 302)
        M100Label.Name = "M100Label"
        M100Label.Size = New System.Drawing.Size(47, 15)
        M100Label.TabIndex = 28
        M100Label.Text = "M100:"
        '
        'M50Label
        '
        M50Label.AutoSize = True
        M50Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M50Label.ForeColor = System.Drawing.Color.LightSlateGray
        M50Label.Location = New System.Drawing.Point(669, 328)
        M50Label.Name = "M50Label"
        M50Label.Size = New System.Drawing.Size(39, 15)
        M50Label.TabIndex = 30
        M50Label.Text = "M50:"
        '
        'M20Label
        '
        M20Label.AutoSize = True
        M20Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M20Label.ForeColor = System.Drawing.Color.LightSlateGray
        M20Label.Location = New System.Drawing.Point(669, 354)
        M20Label.Name = "M20Label"
        M20Label.Size = New System.Drawing.Size(39, 15)
        M20Label.TabIndex = 32
        M20Label.Text = "M20:"
        '
        'M10Label
        '
        M10Label.AutoSize = True
        M10Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M10Label.ForeColor = System.Drawing.Color.LightSlateGray
        M10Label.Location = New System.Drawing.Point(669, 380)
        M10Label.Name = "M10Label"
        M10Label.Size = New System.Drawing.Size(39, 15)
        M10Label.TabIndex = 34
        M10Label.Text = "M10:"
        '
        'M5Label
        '
        M5Label.AutoSize = True
        M5Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M5Label.ForeColor = System.Drawing.Color.LightSlateGray
        M5Label.Location = New System.Drawing.Point(669, 406)
        M5Label.Name = "M5Label"
        M5Label.Size = New System.Drawing.Size(31, 15)
        M5Label.TabIndex = 36
        M5Label.Text = "M5:"
        '
        'M2Label
        '
        M2Label.AutoSize = True
        M2Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M2Label.ForeColor = System.Drawing.Color.LightSlateGray
        M2Label.Location = New System.Drawing.Point(669, 432)
        M2Label.Name = "M2Label"
        M2Label.Size = New System.Drawing.Size(31, 15)
        M2Label.TabIndex = 38
        M2Label.Text = "M2:"
        '
        'M1Label
        '
        M1Label.AutoSize = True
        M1Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M1Label.ForeColor = System.Drawing.Color.LightSlateGray
        M1Label.Location = New System.Drawing.Point(669, 458)
        M1Label.Name = "M1Label"
        M1Label.Size = New System.Drawing.Size(31, 15)
        M1Label.TabIndex = 40
        M1Label.Text = "M1:"
        '
        'M050Label
        '
        M050Label.AutoSize = True
        M050Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M050Label.ForeColor = System.Drawing.Color.LightSlateGray
        M050Label.Location = New System.Drawing.Point(669, 484)
        M050Label.Name = "M050Label"
        M050Label.Size = New System.Drawing.Size(47, 15)
        M050Label.TabIndex = 42
        M050Label.Text = "M050:"
        '
        'M020Label
        '
        M020Label.AutoSize = True
        M020Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M020Label.ForeColor = System.Drawing.Color.LightSlateGray
        M020Label.Location = New System.Drawing.Point(669, 510)
        M020Label.Name = "M020Label"
        M020Label.Size = New System.Drawing.Size(47, 15)
        M020Label.TabIndex = 44
        M020Label.Text = "M020:"
        '
        'M010Label
        '
        M010Label.AutoSize = True
        M010Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M010Label.ForeColor = System.Drawing.Color.LightSlateGray
        M010Label.Location = New System.Drawing.Point(669, 536)
        M010Label.Name = "M010Label"
        M010Label.Size = New System.Drawing.Size(47, 15)
        M010Label.TabIndex = 46
        M010Label.Text = "M010:"
        '
        'M005Label
        '
        M005Label.AutoSize = True
        M005Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M005Label.ForeColor = System.Drawing.Color.LightSlateGray
        M005Label.Location = New System.Drawing.Point(669, 562)
        M005Label.Name = "M005Label"
        M005Label.Size = New System.Drawing.Size(47, 15)
        M005Label.TabIndex = 48
        M005Label.Text = "M005:"
        '
        'ChequesLabel
        '
        ChequesLabel.AutoSize = True
        ChequesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChequesLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ChequesLabel.Location = New System.Drawing.Point(204, 507)
        ChequesLabel.Name = "ChequesLabel"
        ChequesLabel.Size = New System.Drawing.Size(67, 15)
        ChequesLabel.TabIndex = 50
        ChequesLabel.Text = "Cheques:"
        ChequesLabel.Visible = False
        '
        'TarjetaLabel
        '
        TarjetaLabel.AutoSize = True
        TarjetaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TarjetaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TarjetaLabel.Location = New System.Drawing.Point(204, 541)
        TarjetaLabel.Name = "TarjetaLabel"
        TarjetaLabel.Size = New System.Drawing.Size(56, 15)
        TarjetaLabel.TabIndex = 52
        TarjetaLabel.Text = "Tarjeta:"
        TarjetaLabel.Visible = False
        '
        'ReferenciaLabel
        '
        ReferenciaLabel.AutoSize = True
        ReferenciaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ReferenciaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ReferenciaLabel.Location = New System.Drawing.Point(376, 207)
        ReferenciaLabel.Name = "ReferenciaLabel"
        ReferenciaLabel.Size = New System.Drawing.Size(81, 15)
        ReferenciaLabel.TabIndex = 54
        ReferenciaLabel.Text = "Referencia:"
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "Consecutivo", True))
        Me.ConsecutivoTextBox.Enabled = False
        Me.ConsecutivoTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(134, 110)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(97, 21)
        Me.ConsecutivoTextBox.TabIndex = 3
        Me.ConsecutivoTextBox.TabStop = False
        '
        'CONPARCIALESBindingSource
        '
        Me.CONPARCIALESBindingSource.DataMember = "CONPARCIALES"
        Me.CONPARCIALESBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FechaDateTimePicker
        '
        Me.FechaDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONPARCIALESBindingSource, "Fecha", True))
        Me.FechaDateTimePicker.Enabled = False
        Me.FechaDateTimePicker.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaDateTimePicker.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.FechaDateTimePicker.Location = New System.Drawing.Point(134, 137)
        Me.FechaDateTimePicker.Name = "FechaDateTimePicker"
        Me.FechaDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaDateTimePicker.TabIndex = 5
        Me.FechaDateTimePicker.TabStop = False
        '
        'ImporteTextBox
        '
        Me.ImporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ImporteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "Importe", True))
        Me.ImporteTextBox.Enabled = False
        Me.ImporteTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteTextBox.Location = New System.Drawing.Point(333, 627)
        Me.ImporteTextBox.Name = "ImporteTextBox"
        Me.ImporteTextBox.ReadOnly = True
        Me.ImporteTextBox.Size = New System.Drawing.Size(97, 21)
        Me.ImporteTextBox.TabIndex = 9
        Me.ImporteTextBox.TabStop = False
        '
        'NumeroCepsaTextBox
        '
        Me.NumeroCepsaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumeroCepsaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "NumeroCepsa", True))
        Me.NumeroCepsaTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroCepsaTextBox.Location = New System.Drawing.Point(134, 204)
        Me.NumeroCepsaTextBox.Name = "NumeroCepsaTextBox"
        Me.NumeroCepsaTextBox.Size = New System.Drawing.Size(200, 21)
        Me.NumeroCepsaTextBox.TabIndex = 1
        '
        'RecibioTextBox
        '
        Me.RecibioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RecibioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "Recibio", True))
        Me.RecibioTextBox.Enabled = False
        Me.RecibioTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecibioTextBox.Location = New System.Drawing.Point(444, 137)
        Me.RecibioTextBox.Name = "RecibioTextBox"
        Me.RecibioTextBox.Size = New System.Drawing.Size(200, 21)
        Me.RecibioTextBox.TabIndex = 13
        Me.RecibioTextBox.TabStop = False
        '
        'HoraDateTimePicker
        '
        Me.HoraDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.CONPARCIALESBindingSource, "Hora", True))
        Me.HoraDateTimePicker.Enabled = False
        Me.HoraDateTimePicker.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HoraDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.HoraDateTimePicker.Location = New System.Drawing.Point(743, 128)
        Me.HoraDateTimePicker.Name = "HoraDateTimePicker"
        Me.HoraDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.HoraDateTimePicker.TabIndex = 15
        Me.HoraDateTimePicker.TabStop = False
        '
        'B1000TextBox
        '
        Me.B1000TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B1000TextBox.CausesValidation = False
        Me.B1000TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "B1000", True))
        Me.B1000TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B1000TextBox.Location = New System.Drawing.Point(279, 306)
        Me.B1000TextBox.Name = "B1000TextBox"
        Me.B1000TextBox.Size = New System.Drawing.Size(97, 21)
        Me.B1000TextBox.TabIndex = 3
        '
        'B500TextBox
        '
        Me.B500TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B500TextBox.CausesValidation = False
        Me.B500TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "B500", True))
        Me.B500TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B500TextBox.Location = New System.Drawing.Point(279, 332)
        Me.B500TextBox.Name = "B500TextBox"
        Me.B500TextBox.Size = New System.Drawing.Size(97, 21)
        Me.B500TextBox.TabIndex = 4
        '
        'B200TextBox
        '
        Me.B200TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B200TextBox.CausesValidation = False
        Me.B200TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "B200", True))
        Me.B200TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B200TextBox.Location = New System.Drawing.Point(279, 358)
        Me.B200TextBox.Name = "B200TextBox"
        Me.B200TextBox.Size = New System.Drawing.Size(97, 21)
        Me.B200TextBox.TabIndex = 5
        '
        'B100TextBox
        '
        Me.B100TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B100TextBox.CausesValidation = False
        Me.B100TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "B100", True))
        Me.B100TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B100TextBox.Location = New System.Drawing.Point(279, 384)
        Me.B100TextBox.Name = "B100TextBox"
        Me.B100TextBox.Size = New System.Drawing.Size(97, 21)
        Me.B100TextBox.TabIndex = 6
        '
        'B50TextBox
        '
        Me.B50TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B50TextBox.CausesValidation = False
        Me.B50TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "B50", True))
        Me.B50TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B50TextBox.Location = New System.Drawing.Point(279, 410)
        Me.B50TextBox.Name = "B50TextBox"
        Me.B50TextBox.Size = New System.Drawing.Size(97, 21)
        Me.B50TextBox.TabIndex = 7
        '
        'B20TextBox
        '
        Me.B20TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B20TextBox.CausesValidation = False
        Me.B20TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "B20", True))
        Me.B20TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B20TextBox.Location = New System.Drawing.Point(279, 436)
        Me.B20TextBox.Name = "B20TextBox"
        Me.B20TextBox.Size = New System.Drawing.Size(97, 21)
        Me.B20TextBox.TabIndex = 8
        '
        'M100TextBox
        '
        Me.M100TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M100TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M100", True))
        Me.M100TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M100TextBox.Location = New System.Drawing.Point(744, 294)
        Me.M100TextBox.Name = "M100TextBox"
        Me.M100TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M100TextBox.TabIndex = 11
        '
        'M50TextBox
        '
        Me.M50TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M50TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M50", True))
        Me.M50TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M50TextBox.Location = New System.Drawing.Point(744, 320)
        Me.M50TextBox.Name = "M50TextBox"
        Me.M50TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M50TextBox.TabIndex = 12
        '
        'M20TextBox
        '
        Me.M20TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M20TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M20", True))
        Me.M20TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M20TextBox.Location = New System.Drawing.Point(744, 346)
        Me.M20TextBox.Name = "M20TextBox"
        Me.M20TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M20TextBox.TabIndex = 13
        '
        'M10TextBox
        '
        Me.M10TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M10TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M10", True))
        Me.M10TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M10TextBox.Location = New System.Drawing.Point(744, 372)
        Me.M10TextBox.Name = "M10TextBox"
        Me.M10TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M10TextBox.TabIndex = 14
        '
        'M5TextBox
        '
        Me.M5TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M5TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M5", True))
        Me.M5TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M5TextBox.Location = New System.Drawing.Point(744, 398)
        Me.M5TextBox.Name = "M5TextBox"
        Me.M5TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M5TextBox.TabIndex = 15
        '
        'M2TextBox
        '
        Me.M2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M2", True))
        Me.M2TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M2TextBox.Location = New System.Drawing.Point(744, 424)
        Me.M2TextBox.Name = "M2TextBox"
        Me.M2TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M2TextBox.TabIndex = 16
        '
        'M1TextBox
        '
        Me.M1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M1", True))
        Me.M1TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M1TextBox.Location = New System.Drawing.Point(744, 450)
        Me.M1TextBox.Name = "M1TextBox"
        Me.M1TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M1TextBox.TabIndex = 17
        '
        'M050TextBox
        '
        Me.M050TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M050TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M050", True))
        Me.M050TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M050TextBox.Location = New System.Drawing.Point(744, 476)
        Me.M050TextBox.Name = "M050TextBox"
        Me.M050TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M050TextBox.TabIndex = 18
        '
        'M020TextBox
        '
        Me.M020TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M020TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M020", True))
        Me.M020TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M020TextBox.Location = New System.Drawing.Point(744, 502)
        Me.M020TextBox.Name = "M020TextBox"
        Me.M020TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M020TextBox.TabIndex = 19
        '
        'M010TextBox
        '
        Me.M010TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M010TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M010", True))
        Me.M010TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M010TextBox.Location = New System.Drawing.Point(744, 528)
        Me.M010TextBox.Name = "M010TextBox"
        Me.M010TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M010TextBox.TabIndex = 20
        '
        'M005TextBox
        '
        Me.M005TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M005TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "M005", True))
        Me.M005TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M005TextBox.Location = New System.Drawing.Point(744, 554)
        Me.M005TextBox.Name = "M005TextBox"
        Me.M005TextBox.Size = New System.Drawing.Size(97, 21)
        Me.M005TextBox.TabIndex = 21
        '
        'ChequesTextBox
        '
        Me.ChequesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ChequesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "Cheques", True))
        Me.ChequesTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChequesTextBox.Location = New System.Drawing.Point(279, 507)
        Me.ChequesTextBox.Name = "ChequesTextBox"
        Me.ChequesTextBox.Size = New System.Drawing.Size(97, 21)
        Me.ChequesTextBox.TabIndex = 9
        Me.ChequesTextBox.Visible = False
        '
        'TarjetaTextBox
        '
        Me.TarjetaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TarjetaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "Tarjeta", True))
        Me.TarjetaTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TarjetaTextBox.Location = New System.Drawing.Point(279, 533)
        Me.TarjetaTextBox.Name = "TarjetaTextBox"
        Me.TarjetaTextBox.Size = New System.Drawing.Size(97, 21)
        Me.TarjetaTextBox.TabIndex = 10
        Me.TarjetaTextBox.Visible = False
        '
        'ReferenciaTextBox
        '
        Me.ReferenciaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ReferenciaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPARCIALESBindingSource, "Referencia", True))
        Me.ReferenciaTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReferenciaTextBox.Location = New System.Drawing.Point(454, 204)
        Me.ReferenciaTextBox.Name = "ReferenciaTextBox"
        Me.ReferenciaTextBox.Size = New System.Drawing.Size(200, 21)
        Me.ReferenciaTextBox.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CONPARCIALESBindingNavigator)
        Me.Panel1.Controls.Add(ConsecutivoLabel)
        Me.Panel1.Controls.Add(Me.ConsecutivoTextBox)
        Me.Panel1.Controls.Add(Me.ReferenciaTextBox)
        Me.Panel1.Controls.Add(FechaLabel)
        Me.Panel1.Controls.Add(ReferenciaLabel)
        Me.Panel1.Controls.Add(Me.FechaDateTimePicker)
        Me.Panel1.Controls.Add(Me.TarjetaTextBox)
        Me.Panel1.Controls.Add(CajeraLabel)
        Me.Panel1.Controls.Add(TarjetaLabel)
        Me.Panel1.Controls.Add(Me.ChequesTextBox)
        Me.Panel1.Controls.Add(ImporteLabel)
        Me.Panel1.Controls.Add(ChequesLabel)
        Me.Panel1.Controls.Add(Me.ImporteTextBox)
        Me.Panel1.Controls.Add(Me.M005TextBox)
        Me.Panel1.Controls.Add(NumeroCepsaLabel)
        Me.Panel1.Controls.Add(M005Label)
        Me.Panel1.Controls.Add(Me.NumeroCepsaTextBox)
        Me.Panel1.Controls.Add(Me.M010TextBox)
        Me.Panel1.Controls.Add(RecibioLabel)
        Me.Panel1.Controls.Add(M010Label)
        Me.Panel1.Controls.Add(Me.RecibioTextBox)
        Me.Panel1.Controls.Add(Me.M020TextBox)
        Me.Panel1.Controls.Add(HoraLabel)
        Me.Panel1.Controls.Add(M020Label)
        Me.Panel1.Controls.Add(Me.HoraDateTimePicker)
        Me.Panel1.Controls.Add(Me.M050TextBox)
        Me.Panel1.Controls.Add(B1000Label)
        Me.Panel1.Controls.Add(M050Label)
        Me.Panel1.Controls.Add(Me.B1000TextBox)
        Me.Panel1.Controls.Add(Me.M1TextBox)
        Me.Panel1.Controls.Add(B500Label)
        Me.Panel1.Controls.Add(M1Label)
        Me.Panel1.Controls.Add(Me.B500TextBox)
        Me.Panel1.Controls.Add(Me.M2TextBox)
        Me.Panel1.Controls.Add(B200Label)
        Me.Panel1.Controls.Add(M2Label)
        Me.Panel1.Controls.Add(Me.B200TextBox)
        Me.Panel1.Controls.Add(Me.M5TextBox)
        Me.Panel1.Controls.Add(B100Label)
        Me.Panel1.Controls.Add(M5Label)
        Me.Panel1.Controls.Add(Me.B100TextBox)
        Me.Panel1.Controls.Add(Me.M10TextBox)
        Me.Panel1.Controls.Add(B50Label)
        Me.Panel1.Controls.Add(M10Label)
        Me.Panel1.Controls.Add(Me.B50TextBox)
        Me.Panel1.Controls.Add(Me.M20TextBox)
        Me.Panel1.Controls.Add(B20Label)
        Me.Panel1.Controls.Add(M20Label)
        Me.Panel1.Controls.Add(Me.B20TextBox)
        Me.Panel1.Controls.Add(Me.M50TextBox)
        Me.Panel1.Controls.Add(M100Label)
        Me.Panel1.Controls.Add(M50Label)
        Me.Panel1.Controls.Add(Me.M100TextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(974, 660)
        Me.Panel1.TabIndex = 56
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONPARCIALESBindingSource, "Cajera", True))
        Me.ComboBox1.DataSource = Me.MUESTRAUSUARIOS2BindingSource
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(134, 168)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(422, 23)
        Me.ComboBox1.TabIndex = 0
        Me.ComboBox1.ValueMember = "Clv_Usuario"
        '
        'MUESTRAUSUARIOS2BindingSource
        '
        Me.MUESTRAUSUARIOS2BindingSource.DataMember = "MUESTRAUSUARIOS2"
        Me.MUESTRAUSUARIOS2BindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(280, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 24)
        Me.Label3.TabIndex = 58
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(710, 257)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 24)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Monedas:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(203, 257)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 24)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Billetes:"
        '
        'CONPARCIALESBindingNavigator
        '
        Me.CONPARCIALESBindingNavigator.AddNewItem = Nothing
        Me.CONPARCIALESBindingNavigator.BindingSource = Me.CONPARCIALESBindingSource
        Me.CONPARCIALESBindingNavigator.CountItem = Nothing
        Me.CONPARCIALESBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONPARCIALESBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorSeparator2, Me.BindingNavigatorDeleteItem, Me.CONPARCIALESBindingNavigatorSaveItem})
        Me.CONPARCIALESBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONPARCIALESBindingNavigator.MoveFirstItem = Nothing
        Me.CONPARCIALESBindingNavigator.MoveLastItem = Nothing
        Me.CONPARCIALESBindingNavigator.MoveNextItem = Nothing
        Me.CONPARCIALESBindingNavigator.MovePreviousItem = Nothing
        Me.CONPARCIALESBindingNavigator.Name = "CONPARCIALESBindingNavigator"
        Me.CONPARCIALESBindingNavigator.PositionItem = Nothing
        Me.CONPARCIALESBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONPARCIALESBindingNavigator.Size = New System.Drawing.Size(974, 25)
        Me.CONPARCIALESBindingNavigator.TabIndex = 22
        Me.CONPARCIALESBindingNavigator.TabStop = True
        Me.CONPARCIALESBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'CONPARCIALESBindingNavigatorSaveItem
        '
        Me.CONPARCIALESBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONPARCIALESBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONPARCIALESBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONPARCIALESBindingNavigatorSaveItem.Name = "CONPARCIALESBindingNavigatorSaveItem"
        Me.CONPARCIALESBindingNavigatorSaveItem.Size = New System.Drawing.Size(134, 22)
        Me.CONPARCIALESBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(838, 678)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(148, 40)
        Me.Button1.TabIndex = 23
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CONPARCIALESTableAdapter
        '
        Me.CONPARCIALESTableAdapter.ClearBeforeFill = False
        '
        'MUESTRAUSUARIOS2TableAdapter
        '
        Me.MUESTRAUSUARIOS2TableAdapter.ClearBeforeFill = True
        '
        'FrmEntregasParciales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1009, 741)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmEntregasParciales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entregas Parciales"
        CType(Me.CONPARCIALESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.MUESTRAUSUARIOS2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONPARCIALESBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONPARCIALESBindingNavigator.ResumeLayout(False)
        Me.CONPARCIALESBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CONPARCIALESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONPARCIALESTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.CONPARCIALESTableAdapter
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ImporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroCepsaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RecibioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HoraDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents B1000TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B500TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B200TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B100TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B50TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B20TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M100TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M50TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M20TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M10TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M5TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M050TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M020TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M010TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M005TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ChequesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TarjetaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ReferenciaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CONPARCIALESBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CONPARCIALESBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRAUSUARIOS2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAUSUARIOS2TableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRAUSUARIOS2TableAdapter
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
End Class
