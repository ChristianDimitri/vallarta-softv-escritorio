'Imports System.Collections
'Imports System.Web.UI.WebControls
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient


Public Class FrmImprimir
    Private customersByCityReport As ReportDocument
    Dim bloqueado, identi As Integer
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    'Private Const PARAMETER_FIELD_NAME As String = "Op"  

    'Direccion Sucursal
    Dim RCalleSucur As String = Nothing
    Dim RNumSucur As String = Nothing
    Dim RColSucur As String = Nothing
    Dim RMuniSucur As String = Nothing
    Dim RCiudadSucur As String = Nothing
    Dim RCPSucur As String = Nothing
    Dim RTelSucur As String = Nothing

    Private Sub ConfigureCrystalReports_NewXml(ByVal Clv_Factura As Long)
        Try
            Dim cnn As New SqlConnection(MiConexion)

            customersByCityReport = New ReportDocument
            'Dim connectionInfo As New ConnectionInfo



            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\FacturaFiscal.rpt"
            'reportPath = "C:\Users\TeamEdgar\Documents\Visual Studio 2008\Projects\Reportes\Reportes\CrystalReport1.rpt"



            Dim cmd As New SqlCommand("FactFiscales_New", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim parametro1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = Clv_Factura
            cmd.Parameters.Add(parametro1)

            Dim da As New SqlDataAdapter(cmd)

            'Dim data1 As New DataTable()
            'Dim data2 As New DataTable()
            Dim ds As New DataSet()
            da.Fill(ds)



            'Dim cmd2 As New SqlCommand("DetFactFiscales_New ", cnn)
            'cmd2.CommandType = CommandType.StoredProcedure
            'Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            'parametro.Direction = ParameterDirection.Input
            'parametro.Value = Clv_Factura
            'cmd2.Parameters.Add(parametro)

            'Dim da2 As New SqlDataAdapter(cmd2)



            'da.Fill(data1)
            'da2.Fill(data2)

            ds.Tables(0).TableName = "FactFiscales_New"
            ds.Tables(1).TableName = "DetFactFiscales_New"

            'ds.Tables.Add(data1)
            'ds.Tables.Add(data2)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)

            'customersByCityReport.ExportToDisk(ExportFormatType.PortableDocFormat, "C:\Users\TeamEdgar\Documents\mipdf.pdf")
            'customersByCityReport.PrintToPrinter(1, True, 1, 1)

            CrystalReportViewer1.ReportSource = customersByCityReport




            customersByCityReport = Nothing
            System.GC.Collect()
            bnd = True
            'End Using
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
        Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable

        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        'reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)
        CON.Close()

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        DameGralMsjTickets.Connection = CON2
        DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
        CON2.Close()

        'If IdSistema = "AG" And identi > 0 Then
        '    ConfigureCrystalReports_NewXml(Clv_Factura)
        '    ba = True
        '    Exit Sub
        'Else
        ConfigureCrystalReports_tickets(Clv_Factura)
        'Exit Sub

        'End If


        'End If

        'customersByCityReport.Load(reportPath)

        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")
        ''If GloImprimeTickets = True Then
        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If



        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport = Nothing
    End Sub

    Private Sub DamePeridoTicket(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_InformacionTicket", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato
        comando.Parameters.Add(par1)
        Dim par2 As New SqlParameter("@clv_factura", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = GloClv_Factura
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                GloFechaPeridoPagado = reader(0).ToString()

                GloFechaPeriodoFinal = reader(2).ToString()
                GloFechaPeriodoPagadoMes = reader(1).ToString()
                GloFechaProximoPago = reader(3).ToString()
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("ReportesFacturas", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0
        'Dim ds As New DataSet()

        Dim reportPath As String = Nothing

        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"


        'customersByCityReport.Load(reportPath)
        ''If IdSistema <> "TO" Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = GloClv_Factura
        cmd.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_Factura_Ini", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = 0
        cmd.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_Factura_Fin", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 0
        cmd.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = "01/01/1900"
        cmd.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "01/01/1900"
        cmd.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@op", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = 0
        cmd.Parameters.Add(parametro5)

        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()


            da.Fill(ds)
        ds.Tables(0).TableName = "ReportesFacturas"
            ds.Tables(1).TableName = "CALLES"
            ds.Tables(2).TableName = "CatalogoCajas"
            ds.Tables(3).TableName = "CIUDADES"
            ds.Tables(4).TableName = "CLIENTES"
            ds.Tables(5).TableName = "COLONIAS"
            ds.Tables(6).TableName = "DatosFiscales"
            ds.Tables(7).TableName = "DetFacturas"
            ds.Tables(8).TableName = "DetFacturasImpuestos"
            ds.Tables(9).TableName = "Facturas"
            ds.Tables(10).TableName = "GeneralDesconexion"
            ds.Tables(11).TableName = "SUCURSALES"
            ds.Tables(12).TableName = "Usuarios"


        DamePeridoTicket(GloContrato)
        If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
            GloFechaPeridoPagado = "5"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
            GloFechaPeridoPagado = "10"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
            GloFechaPeridoPagado = "15"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
            GloFechaPeridoPagado = "20"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
            GloFechaPeridoPagado = "25"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
            GloFechaPeridoPagado = "1"
        ElseIf GloFechaPeridoPagado = "Periodo : " Then
            GloFechaPeridoPagado = " "
        End If

        consultaDatosGeneralesSucursal(0, GloClv_Factura)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)


        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"

        customersByCityReport.DataDefinition.FormulaFields("DireccionSucursal").Text = "'" & RCalleSucur & " - #" & RNumSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpSucursal").Text = "'" & RColSucur & ", C.P." & RCPSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadSucursal").Text = "'" & RCiudadSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoSucursal").Text = "'" & RTelSucur & "'"

        customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
        customersByCityReport.DataDefinition.FormulaFields("PagoProximo").Text = "'" & GloFechaProximoPago & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport


        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'LiTipo = 0
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport = Nothing

        
    End Sub
    'Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
    '    Dim ba As Boolean = False
    '    Select Case IdSistema
    '        Case "VA"
    '            customersByCityReport = New ReporteCajasTickets_2VA
    '        Case "LO"
    '            customersByCityReport = New ReporteCajasTickets_2Log
    '        Case "AG"
    '            customersByCityReport = New ReporteCajasTickets_2AG
    '        Case "SA"
    '            customersByCityReport = New ReporteCajasTickets_2SA
    '        Case "TO"
    '            customersByCityReport = New ReporteCajasTickets_2TOM
    '        Case Else
    '            customersByCityReport = New ReporteCajasTickets_2OLD
    '    End Select


    '    Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
    '    Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
    '    Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
    '    Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable

    '    Dim connectionInfo As New ConnectionInfo
    '    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
    '    '    "=True;User ID=DeSistema;Password=1975huli")
    '    connectionInfo.ServerName = GloServerName
    '    connectionInfo.DatabaseName = GloDatabaseName
    '    connectionInfo.UserID = GloUserID
    '    connectionInfo.Password = GloPassword

    '    Dim reportPath As String = Nothing

    '    'If GloImprimeTickets = False Then
    '    ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
    '    'Else

    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    busfac.Connection = CON
    '    busfac.Fill(bfac, Clv_Factura, identi)
    '    CON.Close()

    '    eActTickets = False
    '    Dim CON2 As New SqlConnection(MiConexion)
    '    CON2.Open()
    '    DameGralMsjTickets.Connection = CON2
    '    DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
    '    CON2.Close()

    '    'If IdSistema = "TO" And facnormal = True And identi > 0 Then
    '    '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
    '    '    ba = True
    '    'ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
    '    '    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
    '    '    ba = True
    '    'ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
    '    '    'reportPath = RutaReportes + "\ReporteCajasCosmo.rpt"
    '    '    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
    '    '    ba = True
    '    'Else
    '    '    If IdSistema = "VA" Then
    '    '        'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
    '    '    Else
    '    '        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
    '    '    End If
    '    'End If


    '    'End If

    '    'customersByCityReport.Load(reportPath)
    '    'If GloImprimeTickets = False Then
    '    'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
    '    ' End If
    '    SetDBLogonForReport(connectionInfo, customersByCityReport)

    '    '@Clv_Factura 
    '    customersByCityReport.SetParameterValue(0, GloClv_Factura)
    '    '@Clv_Factura_Ini
    '    customersByCityReport.SetParameterValue(1, "0")
    '    '@Clv_Factura_Fin
    '    customersByCityReport.SetParameterValue(2, "0")
    '    '@Fecha_Ini
    '    customersByCityReport.SetParameterValue(3, "01/01/1900")
    '    '@Fecha_Fin
    '    customersByCityReport.SetParameterValue(4, "01/01/1900")
    '    '@op
    '    customersByCityReport.SetParameterValue(5, "0")
    '    'If GloImprimeTickets = True Then
    '    DamePerido(GloContrato)
    '    If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
    '        GloFechaPeridoPagado = "5"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
    '        GloFechaPeridoPagado = "10"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
    '        GloFechaPeridoPagado = "15"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
    '        GloFechaPeridoPagado = "20"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
    '        GloFechaPeridoPagado = "25"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
    '        GloFechaPeridoPagado = "30"
    '    End If
    '    If ba = False Then
    '        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
    '        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("ProximoPago").Text = "'" & GloFechaProximoPago & "'"
    '        If eActTickets = True Then
    '            customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
    '        End If
    '    End If



    '    CrystalReportViewer1.ReportSource = customersByCityReport

    '    If GloOpFacturas = 3 Then
    '        CrystalReportViewer1.ShowExportButton = False
    '        CrystalReportViewer1.ShowPrintButton = False
    '        CrystalReportViewer1.ShowRefreshButton = False
    '    End If
    '    LiTipo = 0
    '    'SetDBLogonForReport2(connectionInfo)
    '    customersByCityReport = Nothing
    'End Sub
    Private Sub DamePerido(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Sp_InformacionTicket", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = GloClv_Factura
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                GloFechaPeridoPagado = reader(0).ToString()
                GloFechaPeriodoPagadoMes = reader(1).ToString()
                GloFechaPeriodoFinal = reader(2).ToString()
                GloFechaProximoPago = reader(3).ToString()
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub

    Public Sub ReporteMesesAdelantados(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime)
        Me.Text = "Reporte de Mensualidades Adelantadas"
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String
            Dim fechas As String = "Del " + FechaIni.ToShortDateString + " al " + FechaFin.ToShortDateString

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\ReportMesesAdelantados.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, FechaIni)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, FechaFin)

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteMesesAdelantados")

            DS = BaseII.ConsultaDS("ReporteMesesAdelantados", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

            customersByCityReport.SetParameterValue(0, FechaIni)
            customersByCityReport.SetParameterValue(1, FechaFin)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudadEmpresa & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Public Sub ReporteIngresosTarjetas(ByVal fechaI As DateTime, ByVal fechaF As DateTime, ByVal opc As Integer, ByVal tipo As String)
        Me.Text = "Reporte de Ingresos por Tarjetas"
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String
            Dim fechas As String = "Del " + fechaI.ToShortDateString + " al " + fechaF.ToShortDateString

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\ReporteTarjetas.rpt"
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()

            BaseII.CreateMyParameter("@fechaI", SqlDbType.DateTime, fechaI)
            BaseII.CreateMyParameter("@fechaF", SqlDbType.DateTime, fechaF)
            BaseII.CreateMyParameter("@tipoTarjeta", SqlDbType.VarChar, tipo)
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, opc)

            Dim listatablas As New List(Of String)
            listatablas.Add("tarjetasCreditoDebito")

            DS = BaseII.ConsultaDS("tarjetasCreditoDebito", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)
            customersByCityReport.SetParameterValue(0, fechaI)
            customersByCityReport.SetParameterValue(1, fechaF)
            customersByCityReport.SetParameterValue(2, tipo)
            customersByCityReport.SetParameterValue(3, opc)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudadEmpresa & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub FrmImprimir_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub


    Private Sub FrmImprimir_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If LiTipo = 2 Then
            If GloClv_Factura = 0 Then Me.Opacity = 0 Else Me.Opacity = 1
            ConfigureCrystalReports(GloClv_Factura)
        ElseIf LiTipo = 3 Then
            ConfigureCrystalReportsNota(GLONOTA)
        ElseIf LiTipo = 4 Then
            ConfigureCrystalReportsOrden(0, "")
        ElseIf LiTipo = 5 Then
            ConfigureCrystalReportsQueja(0, "")
        ElseIf LiTipo = 6 Then
            ConfigureCrystalReports_tickets2(GloClv_Factura)
        ElseIf LiTipo = 7 Then
            ConfigureCrystalReports_NewXml(GloClv_Factura)
        End If
    End Sub

    Private Sub ConfigureCrystalReports_tickets2(ByVal Clv_Factura As Long)
        Try


            Dim ba As Boolean = False
            Select Case IdSistema
                Case "LO"
                    customersByCityReport = New ReporteCajasTickets_2Log

            End Select


            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            '        If GloImprimeTickets = False Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
            'Else
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.BusFacFiscalTableAdapter.Connection = CON
            'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            'CON.Close()
            'CON.Dispose()

            'eActTickets = False
            'Dim CON2 As New SqlConnection(MiConexion)
            'CON2.Open()
            'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            'CON2.Close()
            'CON2.Dispose()

            'If IdSistema = "VA" Then
            '    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'Else
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'End If

            'End If

            'customersByCityReport.Load(reportPath)
            'If GloImprimeTickets = False Then
            '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'End If
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            ''@Clv_Factura_Ini
            'customersByCityReport.SetParameterValue(1, "0")
            ''@Clv_Factura_Fin
            'customersByCityReport.SetParameterValue(2, "0")
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(3, "01/01/1900")
            ''@Fecha_Fin
            'customersByCityReport.SetParameterValue(4, "01/01/1900")
            ''@op
            'customersByCityReport.SetParameterValue(5, "0")

            'If ba = False Then
            '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            '    If eActTickets = True Then
            '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            '    End If
            'End If

            'If facticket = 1 Then
            '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

            'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


            'customersByCityReport.PrintToPrinter(1, True, 1, 1)





            CrystalReportViewer1.ReportSource = customersByCityReport

            'If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowRefreshButton = True
            'End If
            'SetDBLogonForReport2(connectionInfo)
            'customersByCityReport.Dispose()
        Catch ex As Exception
            MsgBox(Err.Description)
            CrystalReportViewer1.Dispose()
            customersByCityReport.Dispose()
            Me.Close()
        End Try
    End Sub


    Private Sub ConfigureCrystalReportsNota(ByVal nota As Long)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport1(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, nota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else
        CrystalReportViewer1.ReportSource = customersByCityReport
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        ' End If

        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub SetDBLogonForReport1(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ConfigureCrystalReportsOrden(ByVal op As String, ByVal Titulo As String)
        Try
            Dim CON As New SqlConnection(MiConexion)
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0
            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"
            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteOrdenes.rpt"

            Dim listatablas As New List(Of String)
            Dim DS As New DataSet
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CObj(0))
            BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, CShort(1))
            BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, gloClave)
            BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, gloClave)
            BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, CLng(OpOrdenar))

            listatablas.Add("ReporteOrdSer")
            listatablas.Add("Comentarios_DetalleOrden")
            listatablas.Add("DameDatosGenerales_2")
            listatablas.Add("DetOrdSer")
            listatablas.Add("Trabajos")
            listatablas.Add("ClientesConElMismoPoste")
            DS = BaseII.ConsultaDS("ReporteOrdSer", listatablas)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(DS)
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReportsQueja(ByVal op As Integer, ByVal Titulo As String)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"
        Dim a As Integer = 0



        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim Impresora_Ordenes As String = Nothing
        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"


        Dim reportPath As String = Nothing

        If IdSistema = "TO" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
        ElseIf IdSistema = "AG" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
        ElseIf IdSistema = "SA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
        ElseIf IdSistema = "VA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
        ElseIf IdSistema = "LO" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
        End If


        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Clv_TipSer int
        customersByCityReport.SetParameterValue(0, Glo_tipSer)
        ',@op1 smallint
        customersByCityReport.SetParameterValue(1, 1)
        ',@op2 smallint
        customersByCityReport.SetParameterValue(2, 0)
        ',@op3 smallint
        customersByCityReport.SetParameterValue(3, 0)
        ',@op4 smallint,
        customersByCityReport.SetParameterValue(4, 0)
        '@op5 smallint
        customersByCityReport.SetParameterValue(5, 0)
        '@op6 smallint
        customersByCityReport.SetParameterValue(6, 0)
        ',@StatusPen bit
        customersByCityReport.SetParameterValue(7, 0)
        ',@StatusEje bit
        customersByCityReport.SetParameterValue(8, 0)
        ',@StatusVis bit,
        customersByCityReport.SetParameterValue(9, 0)
        '@Clv_OrdenIni bigint
        customersByCityReport.SetParameterValue(10, CInt(gloClave))
        ',@Clv_OrdenFin bigint
        customersByCityReport.SetParameterValue(11, CInt(gloClave))
        ',@Fec1Ini Datetime
        customersByCityReport.SetParameterValue(12, "01/01/1900")
        ',@Fec1Fin Datetime,
        customersByCityReport.SetParameterValue(13, "01/01/1900")
        '@Fec2Ini Datetime
        customersByCityReport.SetParameterValue(14, "01/01/1900")
        ',@Fec2Fin Datetime
        customersByCityReport.SetParameterValue(15, "01/01/1900")
        ',@Clv_Trabajo int
        customersByCityReport.SetParameterValue(16, 0)
        ',@Clv_Colonia int
        customersByCityReport.SetParameterValue(17, 0)
        ',@OpOrden int
        customersByCityReport.SetParameterValue(18, OpOrdenar)
        '@Clv_Departamento
        customersByCityReport.SetParameterValue(19, 0)

        customersByCityReport.SetParameterValue(20, 0)
        '@Clv_Departamento
        customersByCityReport.SetParameterValue(21, 0)

        'Titulos de Reporte
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        mySelectFormula = "Quejas " ' & Me.TextBox2.Text
        customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        'Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
        'Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora_Ordenes, a)
        'If a = 1 Then
        '    MsgBox("No se ha asignado una Impresora de Quejas")
        '    Exit Sub
        'Else
        '    customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
        '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'End If

        CrystalReportViewer1.ReportSource = customersByCityReport

        CON.Close()
        customersByCityReport = Nothing
        'Catch ex As System.Exception
        'System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
    End Sub

    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.RCalleSucur = dtDatosGenerales.Rows(0)("calle").ToString
            Me.RNumSucur = dtDatosGenerales.Rows(0)("numero").ToString
            Me.RColSucur = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.RCPSucur = CInt(dtDatosGenerales.Rows(0)("cp").ToString)
            Me.RMuniSucur = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.RCiudadSucur = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.RTelSucur = dtDatosGenerales.Rows(0)("telefono").ToString
        End If
    End Sub
End Class