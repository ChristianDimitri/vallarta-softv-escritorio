Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmImprimirIngresosConcepto

    Private customersByCityReport As ReportDocument

    Private op As String = Nothing
    Private Titulo As String = Nothing
    'Private Const PARAMETER_FIELD_NAME As String = "Op"    

    Private Sub ConfigureCrystalReports()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing
        Ciudades = " Ciudad(es): " + LocCiudades
        If GloBnd_Des_Cont = True Then
            reportPath = RutaReportes + "\ReporteDesgloce_Contrataciones.rpt"
            Titulo = "Desgloce de Contrataciones"
        ElseIf GloBnd_Des_Men = True Then
            reportPath = RutaReportes + "\ReporteDesgloce_Mensualidades.rpt"
            Titulo = "Desgloce de Mensualidades"
        ElseIf LocClientesPagosAdelantados = True Then
            LocClientesPagosAdelantados = False
            reportPath = RutaReportes + "\ClientesconPagosAdelantados.rpt"
            Titulo = "Relación de Clientes con Pagos Adelantados"
        ElseIf LocClientesPagosAdelantados = False Then
            reportPath = RutaReportes + "\DesgloceporConceptos.rpt"
            Titulo = "Relación de Ingresos por Conceptos"
        End If
        Sucursal = " Sucursal: " + GloNomSucursal
        customersByCityReport.Load(reportPath)

        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

        '@FECHA_INI
        customersByCityReport.SetParameterValue(0, eFechaInicial)
        '@FECHA_FIN
        customersByCityReport.SetParameterValue(1, eFechaFinal)
        '@TIPO
        customersByCityReport.SetParameterValue(2, "")
        '@SUCURSAL
        customersByCityReport.SetParameterValue(3, "0")
        '@CAJA
        customersByCityReport.SetParameterValue(4, "0")
        '@CAJERA
        customersByCityReport.SetParameterValue(5, "")
        '@OP
        customersByCityReport.SetParameterValue(6, "0")
        'Clv_Session
        customersByCityReport.SetParameterValue(7, gloClv_Session)




        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)



        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
        GloBnd_Des_Men = False
        GloBnd_Des_Cont = False

    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub


    Private Sub FrmImprimirIngresosConcepto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IdSistema = "AG" Or IdSistema = "VA" Then
            If LocBndrelingporconceptos = True Then
                LocBndrelingporconceptos = False
                LocbndDesPagos = True
                FrmImprimirRepGral.Show()
            End If
        End If
    End Sub

    Private Sub FrmImprimirIngresosConcepto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GloBnd_Des_Cont = True Then
            Me.Text = "Impresión Desgloce de Contrataciones"
        ElseIf GloBnd_Des_Men = True Then
            Me.Text = "Impresión Desgloce de Mensualidades"
        ElseIf LocClientesPagosAdelantados = True Then
            Me.Text = "Impresión Relación de Clientes con Pagos Adelantados"
        Else
            Me.Text = "Impresión Relación de Ingresos por Conceptos"
        End If

        ConfigureCrystalReports()
        Me.CrystalReportViewer1.ShowPrintButton = True
        Me.CrystalReportViewer1.ShowExportButton = True
        Me.CrystalReportViewer1.ShowRefreshButton = False
    End Sub

    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load

    End Sub
End Class
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
'Imports System.Data.SqlClient
'Imports System.Text

'Public Class FrmImprimirIngresosConcepto

'    Private customersByCityReport As ReportDocument

'    Private op As String = Nothing
'    Private Titulo As String = Nothing
'    'Private Const PARAMETER_FIELD_NAME As String = "Op"    

'    Private Sub ConfigureCrystalReports()
'        customersByCityReport = New ReportDocument
'        Dim connectionInfo As New ConnectionInfo
'        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
'        '    "=True;User ID=DeSistema;Password=1975huli")
'        'connectionInfo.ServerName = GloServerName
'        'connectionInfo.DatabaseName = GloDatabaseName
'        'connectionInfo.UserID = GloUserID
'        'connectionInfo.Password = GloPassword
'        Dim reportPath As String = Nothing
'        Dim Titulo As String = Nothing
'        Dim Sucursal As String = Nothing
'        Dim Ciudades As String = Nothing
'        Ciudades = " Ciudad(es): " + LocCiudades
'        If GloBnd_Des_Cont = True Then
'            reportPath = RutaReportes + "\ReporteDesgloce_Contrataciones.rpt"
'            Titulo = "Desgloce de Contrataciones"
'        ElseIf GloBnd_Des_Men = True Then
'            reportPath = RutaReportes + "\ReporteDesgloce_Mensualidades.rpt"
'            Titulo = "Desgloce de Mensualidades"
'        ElseIf LocClientesPagosAdelantados = True Then
'            LocClientesPagosAdelantados = False
'            reportPath = RutaReportes + "\ClientesconPagosAdelantados.rpt"
'            Titulo = "Relación de Clientes con Pagos Adelantados"
'        ElseIf LocClientesPagosAdelantados = False Then
'            reportPath = RutaReportes + "\DesgloceporConceptos.rpt"
'            Titulo = "Relación de Ingresos por Conceptos"
'        End If
'        Sucursal = " Sucursal: " + GloNomSucursal

'        If LocClientesPagosAdelantados = False Then
'            Dim DS As New DataSet
'            DS.Clear()
'            BaseII.limpiaParametros()

'            BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, eFechaInicial)
'            BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, eFechaFinal)
'            BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, "")
'            BaseII.CreateMyParameter("@sucursal", SqlDbType.Int, 0)
'            BaseII.CreateMyParameter("@Caja", SqlDbType.Int, 0)
'            BaseII.CreateMyParameter("@Cajera", SqlDbType.VarChar, "")
'            BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
'            BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, gloClv_Session)

'            Dim listatablas As New List(Of String)
'            listatablas.Add("Desgloce_de_Prueba")
'            listatablas.Add("TipServ")

'            DS = BaseII.ConsultaDS("Desgloce_de_Prueba", listatablas)

'            customersByCityReport.Load(reportPath)
'            SetDBReport(DS, customersByCityReport)
'        Else
'            customersByCityReport.Load(reportPath)

'            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
'            SetDBLogonForReport(connectionInfo, customersByCityReport)
'            'SetDBLogonForReport(connectionInfo, customersByCityReport)
'            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
'        End If


'        '@FECHA_INI
'        customersByCityReport.SetParameterValue(0, eFechaInicial)
'        '@FECHA_FIN
'        customersByCityReport.SetParameterValue(1, eFechaFinal)
'        '@TIPO
'        customersByCityReport.SetParameterValue(2, "")
'        '@SUCURSAL
'        customersByCityReport.SetParameterValue(3, "0")
'        '@CAJA
'        customersByCityReport.SetParameterValue(4, "0")
'        '@CAJERA
'        customersByCityReport.SetParameterValue(5, "")
'        '@OP
'        customersByCityReport.SetParameterValue(6, "0")
'        'Clv_Session
'        customersByCityReport.SetParameterValue(7, gloClv_Session)



'        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
'        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
'        eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
'        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
'        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
'        customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


'        CrystalReportViewer1.ReportSource = customersByCityReport
'        CrystalReportViewer1.Zoom(75)



'        If GloOpFacturas = 3 Then
'            CrystalReportViewer1.ShowExportButton = False
'            CrystalReportViewer1.ShowPrintButton = False
'            CrystalReportViewer1.ShowRefreshButton = False
'        End If
'        'SetDBLogonForReport2(connectionInfo)
'        customersByCityReport = Nothing
'        GloBnd_Des_Men = False
'        GloBnd_Des_Cont = False

'    End Sub

'    Private Sub ReporteRelacionDeIngresosPorConceptos()
'        customersByCityReport = New ReportDocument
'        Dim conexion As New SqlConnection(MiConexion)
'        Dim sBuilder As New StringBuilder("EXEC ReporteRelacionDeIngresosPorConceptos '" + eFechaInicial + "', '" + eFechaFinal + "', " + gloClv_Session.ToString)
'        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
'        Dim dSet As New DataSet
'        Dim reportPath As String = Nothing
'        Dim Titulo As String = Nothing
'        Dim Sucursal As String = Nothing
'        Dim Ciudades As String = Nothing

'        Ciudades = " Ciudad(es): " + LocCiudades
'        Titulo = "Relación de Ingresos por Conceptos"
'        Sucursal = " Sucursal: " + GloNomSucursal

'        Try

'            dAdapter.Fill(dSet)
'            dSet.Tables(0).TableName = "Desgloce_de_Prueba"
'            dSet.Tables(1).TableName = "TipServ"

'            reportPath = RutaReportes + "\RelacionDeIngresosPorConceptos.rpt"
'            customersByCityReport.Load(reportPath)
'            customersByCityReport.SetDataSource(dSet)

'            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
'            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
'            eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
'            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
'            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
'            customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


'            CrystalReportViewer1.ReportSource = customersByCityReport
'            CrystalReportViewer1.Zoom(75)


'            Me.CrystalReportViewer1.ShowPrintButton = True
'            Me.CrystalReportViewer1.ShowExportButton = True
'            Me.CrystalReportViewer1.ShowRefreshButton = False

'            If GloOpFacturas = 3 Then
'                CrystalReportViewer1.ShowExportButton = False
'                CrystalReportViewer1.ShowPrintButton = False
'                CrystalReportViewer1.ShowRefreshButton = False
'            End If

'            customersByCityReport = Nothing
'            GloBnd_Des_Men = False
'            GloBnd_Des_Cont = False

'        Catch ex As Exception

'        End Try

'    End Sub

'    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
'        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
'        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

'        Dim myTables As Tables = myReportDocument.Database.Tables
'        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
'        For Each myTable In myTables
'            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
'            myTableLogonInfo.ConnectionInfo = myConnectionInfo
'            myTable.ApplyLogOnInfo(myTableLogonInfo)
'            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
'        Next
'    End Sub

'    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
'        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
'        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

'        Dim I As Integer = myReportDocument.Subreports.Count
'        Dim X As Integer = 0
'        For X = 0 To I - 1
'            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
'            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
'            For Each myTable In myTables
'                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
'                myTableLogonInfo.ConnectionInfo = myConnectionInfo
'                myTable.ApplyLogOnInfo(myTableLogonInfo)
'                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
'            Next
'        Next X
'    End Sub

'    Private Sub FrmImprimirIngresosConcepto_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

'    End Sub

'    Private Sub FrmImprimirIngresosConcepto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
'        If IdSistema = "AG" Or IdSistema = "VA" Then
'            If LocBndrelingporconceptos = True Then
'                LocBndrelingporconceptos = False
'                LocbndDesPagos = True
'                FrmImprimirRepGral.Show()
'            End If
'        End If
'    End Sub

'    Private Sub FrmImprimirIngresosConcepto_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.HandleDestroyed

'    End Sub

'    Private Sub FrmImprimirIngresosConcepto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        If GloBnd_Des_Cont = True Then
'            Me.Text = "Impresión Desgloce de Contrataciones"
'        ElseIf GloBnd_Des_Men = True Then
'            Me.Text = "Impresión Desgloce de Mensualidades"
'        ElseIf LocClientesPagosAdelantados = True Then
'            Me.Text = "Impresión Relación de Clientes con Pagos Adelantados"
'        Else
'            Me.Text = "Impresión Relación de Ingresos por Conceptos"
'            'ReporteRelacionDeIngresosPorConceptos()
'            'Exit Sub
'        End If

'        ConfigureCrystalReports()
'        Me.CrystalReportViewer1.ShowPrintButton = True
'        Me.CrystalReportViewer1.ShowExportButton = True
'        Me.CrystalReportViewer1.ShowRefreshButton = False

'    End Sub

'    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load

'    End Sub
'End Class