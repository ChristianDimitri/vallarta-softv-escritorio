Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmFAC
    Private customersByCityReport As ReportDocument
    Dim BndError As Integer = 0
    Dim Loc_Clv_Vendedor As Integer = 0
    Dim Loc_Folio As Long = 0
    Dim Loc_Serie As String = Nothing
    Dim Msg As String = Nothing
    Dim bloqueado, identi As Integer
    Dim Bnd As Integer = 0
    Dim CuantasTv As Integer = 0
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Dim SiPagos As Integer = 0
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    Private eCont As Integer = 1
    Private eRes As Integer = 0
    Private ePideAparato As Integer = 0
    Private eClv_Detalle As Long = 0
    Private eBnd As Boolean = False
    Private ImprTermi As Integer = 0
    Dim zonas2 As Boolean = False

    'Direccion Sucursal
    Dim RCalleSucur As String = Nothing
    Dim RNumSucur As String = Nothing
    Dim RColSucur As String = Nothing
    Dim RMuniSucur As String = Nothing
    Dim RCiudadSucur As String = Nothing
    Dim RCPSucur As String = Nothing
    Dim RTelSucur As String = Nothing



    Private Sub Guarda_Tipo_Tarjeta(ByVal Clv_factura As Long, ByVal Tipo As Integer, ByVal Monto As Decimal)
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()

        Try
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "Nuevo_Rel_Pago_Tarjeta_Factura"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clv_factura
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Tipo", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Tipo
                .Parameters.Add(prm1)

                prm = New SqlParameter("@Monto", SqlDbType.Money)
                prm.Direction = ParameterDirection.Input
                prm.Value = Monto
                .Parameters.Add(prm)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CON100.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub QUITARDELDETALLE(ByVal MClv_Session As Long, ByVal mClv_Detalle As Long, ByVal mIdSistema As String, ByVal mCLV_TIPOCLIENTE As Integer, ByVal MCONTRATO As Long)
        Dim mERROR As Integer = 0
        Dim MMSGERROR As String = ""
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        '@Clv_Session, @Clv_Detalle, @IdSistema, @CLV_TIPOCLIENTE, @ERROR, @MSGERROR
        Try
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "QUITARDELDETALLE"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = MClv_Session
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Clv_Detalle", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = mClv_Detalle
                .Parameters.Add(prm1)

                prm = New SqlParameter("@IdSistema", SqlDbType.VarChar, 5)
                prm.Direction = ParameterDirection.Input
                prm.Value = mIdSistema
                .Parameters.Add(prm)

                prm = New SqlParameter("@CLV_TIPOCLIENTE", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = mCLV_TIPOCLIENTE
                .Parameters.Add(prm)



                Dim prm4 = New SqlParameter("@ERROR", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = mERROR
                .Parameters.Add(prm4)

                Dim prm3 As New SqlParameter("@MSGERROR", SqlDbType.VarChar, 250)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = MMSGERROR
                .Parameters.Add(prm3)


                Dim prm2 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = MCONTRATO
                .Parameters.Add(prm2)

                Dim ia As Integer = .ExecuteNonQuery()
                BndError = prm4.Value
                Msg = prm3.Value
            End With
            CON100.Close()
        Catch ex As Exception
            If CON100.State <> ConnectionState.Closed Then CON100.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub





    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    'Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
    ' Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
    '     For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
    '         myTableLogOnInfo.ConnectionInfo = myConnectionInfo
    '     Next
    ' End Sub

    Private Sub ConfigureCrystalReports_NewXml(ByVal Clv_Factura As Long)
        Try
            Dim cnn As New SqlConnection(MiConexion)

            customersByCityReport = New ReportDocument


            'Dim connectionInfo As New ConnectionInfo



            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\FacturaFiscal.rpt"
            'reportPath = "C:\Users\TeamEdgar\Documents\Visual Studio 2008\Projects\Reportes\Reportes\CrystalReport1.rpt"



            Dim cmd As New SqlCommand("FactFiscales_New", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim parametro1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = Clv_Factura
            cmd.Parameters.Add(parametro1)

            Dim da As New SqlDataAdapter(cmd)

            'Dim data1 As New DataTable()
            'Dim data2 As New DataTable()





            'Dim cmd2 As New SqlCommand("DetFactFiscales_New ", cnn)
            'cmd2.CommandType = CommandType.StoredProcedure
            'Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            'parametro.Direction = ParameterDirection.Input
            'parametro.Value = Clv_Factura
            'cmd2.Parameters.Add(parametro)

            'Dim da2 As New SqlDataAdapter(cmd2)


            Dim ds As New DataSet()

            da.Fill(ds)


            ds.Tables(0).TableName = "FactFiscales_New"
            ds.Tables(1).TableName = "DetFactFiscales_New"

            'ds.Tables.Add(data1)
            'ds.Tables.Add(data2)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)

            'customersByCityReport.ExportToDisk(ExportFormatType.PortableDocFormat, "C:\Users\TeamEdgar\Documents\mipdf.pdf")
            'customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            eCont = 1
            eRes = 0
            Do
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/3, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
                '6=Yes;7=No
                If eRes = 6 Then eCont = eCont + 1

            Loop While eCont <= 3

            'CrystalReportViewer1.ReportSource = customersByCityReport




            customersByCityReport = Nothing
            Bnd = True
            System.GC.Collect()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)

        'If BUSFACFISCAL(Clv_Factura) > 0 Then
        '    REPORTEFacturaFiscal(Clv_Factura)
        'Else
        ConfigureCrystalReports_tickets(Clv_Factura, "Original")

        usp_ImpresoraTermica()
        If ImprTermi = 1 Then
            ConfigureCrystalReports_tickets(Clv_Factura, "Copia")
        End If

        'End If

        'Dim ba As Boolean = False
        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo

        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.BusFacFiscalTableAdapter.Connection = CON
        'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
        'CON.Dispose()
        'CON.Close()

        'eActTickets = False
        'Dim CON2 As New SqlConnection(MiConexion)
        'CON2.Open()
        'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        'CON2.Close()

        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True
        'ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
        '    ba = True
        'ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
        '    ba = True
        'ElseIf identi = 0 Then
        '    If IdSistema = "VA" Then
        '        'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
        '        ConfigureCrystalReports_tickets(Clv_Factura)
        '        Exit Sub
        '    Else
        '        ConfigureCrystalReports_tickets(Clv_Factura)
        '        Exit Sub
        '    End If
        'End If

        'customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)


        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, Clv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If

        'If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then

        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else
        '    customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        'End If

        'If IdSistema = "AG" And facnormal = True And identi > 0 Then
        '    eCont = 1
        '    eRes = 0
        '    Do
        '        If (MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
        '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '            eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")

        '            If eRes = 6 Then eCont = eCont + 1
        '        Else
        '            Exit Sub
        '        End If
        '    Loop While eCont <= 1
        '    eCont = 1
        '    eRes = 0
        '    Do
        '        If (MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
        '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '            eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 1) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
        '            '6=Yes;7=No
        '            If eRes = 6 Then eCont = eCont + 1
        '        Else
        '            Exit Sub
        '        End If
        '    Loop While eCont <= 1
        '    eCont = 1
        '    eRes = 0
        '    Do
        '        If (MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
        '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '            eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 2) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
        '            '6=Yes;7=No
        '            If eRes = 6 Then eCont = eCont + 1
        '        Else
        '            Exit Sub
        '        End If
        '    Loop While eCont <= 1
        '    eCont = 1
        '    eRes = 0
        '    Do
        '        If (MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
        '            customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '            eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 3) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
        '            '6=Yes;7=No
        '            If eRes = 6 Then eCont = eCont + 1
        '        Else
        '            Exit Sub
        '        End If
        '    Loop While eCont <= 1
        'Else
        '    If IdSistema = "SA" Then
        '        MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
        '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '        MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
        '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '    Else
        '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        '    End If

        'End If

        'customersByCityReport = Nothing

    End Sub

    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long, ByVal oMsj As String)
        customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("ReportesFacturas", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim reportPath As String = Nothing

        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"


        'customersByCityReport.Load(reportPath)
        ''If IdSistema <> "TO" Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = GloClv_Factura
        cmd.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Clv_Factura_Ini", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = 0
        cmd.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Clv_Factura_Fin", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 0
        cmd.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = "01/01/1900"
        cmd.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "01/01/1900"
        cmd.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@op", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = 0
        cmd.Parameters.Add(parametro5)

        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()


        da.Fill(ds)
        ds.Tables(0).TableName = "ReportesFacturas"
        ds.Tables(1).TableName = "CALLES"
        ds.Tables(2).TableName = "CatalogoCajas"
        ds.Tables(3).TableName = "CIUDADES"
        ds.Tables(4).TableName = "CLIENTES"
        ds.Tables(5).TableName = "COLONIAS"
        ds.Tables(6).TableName = "DatosFiscales"
        ds.Tables(7).TableName = "DetFacturas"
        ds.Tables(8).TableName = "DetFacturasImpuestos"
        ds.Tables(9).TableName = "Facturas"
        ds.Tables(10).TableName = "GeneralDesconexion"
        ds.Tables(11).TableName = "SUCURSALES"
        ds.Tables(12).TableName = "Usuarios"


        'DamePerido(GloContrato)
        DamePeridoTicket(Clv_Factura)
        If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
            GloFechaPeridoPagado = "5"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
            GloFechaPeridoPagado = "10"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
            GloFechaPeridoPagado = "15"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
            GloFechaPeridoPagado = "20"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
            GloFechaPeridoPagado = "25"
        ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
            GloFechaPeridoPagado = "1"
        ElseIf GloFechaPeridoPagado = "Periodo : " Then
            GloFechaPeridoPagado = " "
        End If

        consultaDatosGeneralesSucursal(0, GloClv_Factura)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"

        customersByCityReport.DataDefinition.FormulaFields("DireccionSucursal").Text = "'" & RCalleSucur & " - #" & RNumSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpSucursal").Text = "'" & RColSucur & ", C.P." & RCPSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadSucursal").Text = "'" & RCiudadSucur & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoSucursal").Text = "'" & RTelSucur & "'"

        'customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'" & oMsj & "'"
        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
        customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
        customersByCityReport.DataDefinition.FormulaFields("PagoProximo").Text = "'" & GloFechaProximoPago & "'"

        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        customersByCityReport.Dispose()

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub

    'Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
    '    Dim ba As Boolean = False
    '    Select Case IdSistema
    '        Case "VA"
    '            customersByCityReport = New ReporteCajasTickets_2VA
    '        Case "LO"
    '            customersByCityReport = New ReporteCajasTickets_2Log
    '        Case "AG"
    '            customersByCityReport = New ReporteCajasTickets_2AG
    '        Case "SA"
    '            customersByCityReport = New ReporteCajasTickets_2SA
    '        Case "TO"
    '            customersByCityReport = New ReporteCajasTickets_2TOM
    '        Case Else
    '            customersByCityReport = New ReporteCajasTickets_2OLD
    '    End Select

    '    Dim connectionInfo As New ConnectionInfo

    '    connectionInfo.ServerName = GloServerName
    '    connectionInfo.DatabaseName = GloDatabaseName
    '    connectionInfo.UserID = GloUserID
    '    connectionInfo.Password = GloPassword

    '    Dim reportPath As String = Nothing

    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    Me.BusFacFiscalTableAdapter.Connection = CON
    '    Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
    '    CON.Close()
    '    CON.Dispose()

    '    eActTickets = False
    '    Dim CON2 As New SqlConnection(MiConexion)
    '    CON2.Open()
    '    Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
    '    Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
    '    CON2.Close()
    '    CON2.Dispose()

    '    SetDBLogonForReport(connectionInfo, customersByCityReport)

    '    '@Clv_Factura 
    '    customersByCityReport.SetParameterValue(0, Clv_Factura)
    '    '@Clv_Factura_Ini
    '    customersByCityReport.SetParameterValue(1, "0")
    '    '@Clv_Factura_Fin
    '    customersByCityReport.SetParameterValue(2, "0")
    '    '@Fecha_Ini
    '    customersByCityReport.SetParameterValue(3, "01/01/1900")
    '    '@Fecha_Fin
    '    customersByCityReport.SetParameterValue(4, "01/01/1900")
    '    '@op
    '    customersByCityReport.SetParameterValue(5, "0")

    '    DamePeridoTicket(GloContrato)
    '    If GloFechaPeridoPagado = "Periodo : Periodo 5" Then
    '        GloFechaPeridoPagado = "5"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 10" Then
    '        GloFechaPeridoPagado = "10"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 15" Then
    '        GloFechaPeridoPagado = "15"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 20" Then
    '        GloFechaPeridoPagado = "20"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 25" Then
    '        GloFechaPeridoPagado = "25"
    '    ElseIf GloFechaPeridoPagado = "Periodo : Periodo 30" Then
    '        GloFechaPeridoPagado = "1"
    '    ElseIf GloFechaPeridoPagado = "Periodo : " Then
    '        GloFechaPeridoPagado = ""
    '    End If
    '    If ba = False Then
    '        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("Periodo").Text = "'" & GloFechaPeridoPagado & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("PeriodoMes").Text = "'" & GloFechaPeriodoPagadoMes & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("PeriodoFin").Text = "'" & GloFechaPeriodoFinal & "'"
    '        customersByCityReport.DataDefinition.FormulaFields("ProximoPago").Text = "'" & GloFechaProximoPago & "'"
    '        If eActTickets = True Then
    '            customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
    '        End If
    '    End If

    '    customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
    '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
    '    customersByCityReport.Dispose()
    'End Sub
    Private Sub DamePeridoTicket(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_InformacionTicket", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato
        comando.Parameters.Add(par1)
        Dim par2 As New SqlParameter("@clv_factura", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = GloClv_Factura
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                GloFechaPeridoPagado = reader(0).ToString()
                Label26.Text = reader(1).ToString()
                GloFechaPeriodoFinal = reader(2).ToString()
                GloFechaPeriodoPagadoMes = Label26.Text
                GloFechaProximoPago = reader(3).ToString()
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub borracambiosdeposito(ByVal clv_session As Long, ByVal contrato As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        If locbndborracabiosdep = True Then
            locbndborracabiosdep = False
            cmd = New SqlClient.SqlCommand
            con.Open()
            With cmd
                .Connection = con
                .CommandText = "Deshacerdepositocancela"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_session bigint,@contrato bigint
                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input

                prm.Value = CLng(clv_session)
                prm1.Value = CLng(contrato)

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            con.Close()
            con.Dispose()
        End If
    End Sub
    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim clv_Session1 As Long = 0
        Dim parcial As Integer
        Dim Liperiodo As String
        Try
            'If locbndborracabiosdep = True Then
            '    'locbndborracabiosdep = False
            '    borracambiosdeposito(locclvsessionpardep, Loccontratopardep)
            'End If
            'locbndborracabiosdep = False
            SiPagos = 0
            Bnd = 0
            CuantasTv = 0
            'gloClv_Session = 0
            'GloContrato = 0
            Me.Panel5.Visible = False
            Me.CMBPanel6.Visible = False
            BndError = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Session.Text) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
                'CON.Close()
            End If
            If IsNumeric(gloClv_Session) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
                'CON.Close()
            End If
            If IsNumeric(clv_Session1) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(clv_Session1, Long)))
                'CON.Close()
            End If

            gloClv_Session = 0
            clv_Session1 = 0

            If IsNumeric(GloContrato) = True Then

                ConRelClienteObs(GloContrato)
                'GloContrato = Me.ContratoTextBox.Text
                'Glocontratosel = Me.ContratoTextBox.Text
                'Dim CON2 As New SqlConnection(MiConexion)
                'CON2.Open()
                Dim comando As SqlClient.SqlCommand

                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, GloContrato, 0)
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.NewsoftvDataSet2.BuscaBloqueado, GloContrato, num, num2)
                'Me.Dime_Si_DatosFiscalesTableAdapter.Connection = CON
                'Me.Dime_Si_DatosFiscalesTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Si_DatosFiscales, GloContrato, parcial)
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Dime_Si_DatosFiscales "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@cont", SqlDbType.Int)
                    Dim prm2 As New SqlParameter("@Periodo", SqlDbType.VarChar, 50)
                    Dim prm3 As New SqlParameter("@al_corriente", SqlDbType.VarChar, 100)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm.Value = GloContrato
                    prm1.Value = 0
                    prm2.Value = 0
                    prm3.Value = ""
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    'If IdSistema = "VA" Then
                    .Parameters.Add(prm3)

                    Dim i As Integer = comando.ExecuteNonQuery()
                    parcial = prm1.Value
                    Liperiodo = prm2.Value

                    Me.REDLabel26.Text = prm3.Value


                End With

                If parcial > 0 Then
                    Me.REDLabel25.Visible = True
                    Me.REDLabel25.Text = " EL CLIENTE CUENTA CON DATOS FISCALES "
                Else
                    Me.REDLabel25.Visible = False
                End If
                If Liperiodo <> "" Then
                    Me.Label25.Text = Liperiodo
                Else
                    Me.Label25.Text = ""
                End If
                If IdSistema = "VA" And Me.REDLabel26.Text.Trim <> "SN" Then
                    Me.REDLabel26.Visible = True
                ElseIf IdSistema <> "VA" Then
                    Me.REDLabel26.Visible = False
                End If
                'CON2.Close()
                If num = 0 Or num2 = 0 Then
                    'CREAARBOL()
                    If Liperiodo <> "" Then
                        Me.Label25.Text = Liperiodo
                    Else
                        Me.Label25.Text = ""
                    End If
                Else
                    bloqueado = 1
                    clibloqueado()
                    MsgBox("El Cliente " + Me.ContratoTextBox.Text + " Ha Sido Bloqueado por lo que no se Podr� Llevar a cabo la Queja ", MsgBoxStyle.Exclamation)
                    Me.ContratoTextBox.Text = 0
                    GloContrato = 0
                    Glocontratosel = 0
                    Me.Clv_Session.Text = 0
                    BUSCACLIENTES(0)
                    Bloque(False)
                    'Glocontratosel = 0
                End If
                If IdSistema = "SA" Then
                    If GloTipo = "V" Then
                        If IsDate(Fecha_Venta.Text) = True Then
                            'Dim CON3 As New SqlConnection(MiConexion)
                            'CON3.Open()
                            
                            Me.Cobra_VentasTableAdapter.Connection = CON
                            Me.Cobra_VentasTableAdapter.Fill(Me.DataSetEdgar.Cobra_Ventas, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg, Fecha_Venta.Value, "V")
                            gloClv_Session = clv_Session1
                            'vALIDA_PAGO_PROPORCIONAL()
                            'CON3.Close()
                        End If
                    Else
                        'Dim CON4 As New SqlConnection(MiConexion)
                        'CON4.Open()
                        
                        Me.CobraTableAdapter.Connection = CON
                        Me.CobraTableAdapter.Connection.CreateCommand.CommandTimeout = 0
                        Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                        YaTengoClv_Session(clv_Session1)
                        gloClv_Session = clv_Session1
                        'vALIDA_PAGO_PROPORCIONAL()
                        'CON4.Close()
                    End If
                Else
                    'Dim CON5 As New SqlConnection(MiConexion)
                    'CON5.Open()
                   
                    Me.CobraTableAdapter.Connection = CON
                    Me.CobraTableAdapter.Connection.CreateCommand.CommandTimeout = 0
                    Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                    YaTengoClv_Session(clv_Session1)
                    gloClv_Session = clv_Session1
                    'vALIDA_PAGO_PROPORCIONAL()
                    ' ChecaAdeudoDeMaterial(GloContrato)
                    'If Parcialidades = 1 Then
                    '    FrmAdeudoDeCable.Show()
                    'End If
                    'CON5.Close()
                End If
                Me.Clv_Session.Text = clv_Session1
                gloClv_Session = clv_Session1
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                    Me.Button2.Enabled = False
                ElseIf bloqueado <> 1 Then
                    Me.Bloque(True)
                    bloqueado = 0
                End If




                'locPregunta = ""
                'locPregunta = UspHaz_Pregunta(GloContrato, 0)
                'If locPregunta.ToString.Length > 0 Then
                '    Dim selpregunta As New FrmPregunta()
                '    Dim oResul As New DialogResult
                '    selpregunta.TextBox1.Text = locPregunta
                '    oResul = selpregunta.ShowDialog()
                '    If oResul = Windows.Forms.DialogResult.OK Then

                '        SP_DAMEPREDETFACTURAS_PARA_ADELANAR(gloClv_Session)
                '        GloAdelantados = 2
                '        Adelantar_Pago()
                '        'GloBnd = True
                '    End If

                'End If
                'Dim CON6 As New SqlConnection(MiConexion)
                'CON6.Open()
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Connection = CON
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Fill(Me.DataSetEdgar.Dime_Si_ProcedePagoParcial, New System.Nullable(Of Long)(CType(Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(GloContrato, Long)), Bnd, CuantasTv)
                'CON6.Close()

                'Comentado el 02 de Dic de 2011
                'If Bnd = 1 And CuantasTv > 0 Then
                '    CMBPanel6.Visible = True
                'End If

                If Loccontratopardep <> CLng(GloContrato) Then
                    Dim cmdarn As New SqlClient.SqlCommand
                    cmdarn = New SqlCommand
                    With cmdarn
                        .Connection = CON
                        .CommandText = "Dime_si_procedepagoparcialdeposito"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        '@clv_session bigint,@contrato bigint,@bnd1 int output,@bnd2 int output
                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@bnd1", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@bnd2", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Output
                        prm3.Direction = ParameterDirection.Output
                        prm.Value = CLng(Clv_Session.Text)
                        prm1.Value = CLng(GloContrato)
                        prm2.Value = 0
                        prm3.Value = 0
                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        Dim i As Integer = cmdarn.ExecuteNonQuery()
                        bnd1pardep1 = prm2.Value
                        bnd1pardep2 = prm3.Value
                    End With
                    locclvsessionpardep = CLng(Clv_Session.Text)
                    Loccontratopardep = CLng(GloContrato)
                    If bnd1pardep1 = 1 Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        'FrmPagosNet.Show()
                    End If
                    If bnd1pardep2 = 1 And bnd2pardep = False Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        'FrmPagosNet.Show()
                    End If
                End If

                'MENSAJE POR ORDENES PENDIENTES DE RETIRO DE APARATO
                ChecaOrdenRetiro(GloContrato)

                'CHECA SI LA PROMOCI�N ES DE CONTRATACI�N

                If IsNumeric(Clv_Session.Text) = False Then
                    Exit Sub
                End If

                ChecaPromocion(Clv_Session.Text)

                If eRes = 1 Then
                    CMBPanel6.Visible = False
                    Dim r As Integer = 0
                    r = MsgBox(eMsj, MsgBoxStyle.YesNo)
                    If r = 7 Then
                        AplazaElPagoPromocion(Clv_Session.Text)
                        Dim CON9 As New SqlConnection(MiConexion)
                        CON9.Open()
                        Me.DameDetalleTableAdapter.Connection = CON9
                        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Clv_Session.Text, 0)
                        Me.SumaDetalleTableAdapter.Connection = CON9
                        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Clv_Session.Text, False, 0)
                        CON9.Dispose()
                        CON9.Close()
                        'Comentado el 02 de Dic de 2011
                        'Else
                        'if Bnd = 1 And CuantasTv > 0 then
                        '    CMBPanel6.Visible = True
                        'End If
                    End If
                End If

            Else
                GloContrato = 0
                'Dim CON7 As New SqlConnection(MiConexion)
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, 0, 0)
                GloDimProp = 0
                Me.CobraTableAdapter.Connection = CON
                Me.CobraTableAdapter.Connection.CreateCommand.CommandTimeout = 0
                Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(0, Long)), "", 0, Me.Clv_Session.Text, BndError, Msg)
                YaTengoClv_Session(Me.Clv_Session.Text)
                'CON7.Close()
                Me.Clv_Session.Text = 0
                Me.TreeView1.Nodes.Clear()
                Me.Bloque(False)
            End If
            CON.Dispose()
            CON.Close()
            DAMETIPOSCLIENTEDAME()
            CREAARBOL()
            bloqueado = 0
        Catch ex As System.Exception
            ' System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub vALIDA_PAGO_PROPORCIONAL()
        GloDimProp = 0
        GloDimProp = SP_DIMECOBRAPROPORCIONALES(GloContrato)
        If GloDimProp = 1 Then
            locPregunta = ""
            locPregunta = "� Deseas Cobrar el Proporcional ?"
            If locPregunta.ToString.Length > 0 Then
                Dim selpregunta As New FrmPregunta()
                Dim oResul As New DialogResult
                selpregunta.TextBox1.Text = locPregunta
                oResul = selpregunta.ShowDialog()
                If oResul = Windows.Forms.DialogResult.No Then
                    SP_DIMESICOBRAPROP(gloClv_Session)
                    SP_DAMEPREDETFACTURAS_PARA_ADELANAR(gloClv_Session)
                    GloAdelantados = 1
                    Adelantar_Pago()
                    'GloBnd = True
                End If

            End If
        End If
    End Sub

    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            Dim CON15 As New SqlConnection(MiConexion)
            CON15.Open()

            If IsNumeric(GloContrato) = True Then
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(GloContrato, Long)))
            Else
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            'CON15.Dispose()
            'CON15.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON15.Dispose()
            CON15.Close()

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ContratoTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ContratoTextBox.KeyDown
        If IsNumeric(ContratoTextBox.Text) = False Then Exit Sub
        Me.Clv_Session.Text = 0
        GloContrato = 0
        'BUSCACLIENTES(0)
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        ElseIf e.KeyValue = 8 Or e.KeyValue = Keys.Delete Then
            If Me.DataGridView1.RowCount > 0 Then
                Me.Clv_Session.Text = 0
                GloContrato = 0
                BUSCACLIENTES(0)
            End If
        ElseIf e.KeyValue = Keys.Enter Then
            If bloqueado <> 1 Then
                If (Me.ContratoTextBox.Text) = "" Then
                    LiContrato = 0
                    Me.Label25.Text = ""
                    Me.REDLabel26.Visible = False
                ElseIf IsNumeric(Me.ContratoTextBox.Text) = True Then
                    LiContrato = Me.ContratoTextBox.Text
                    GloContrato = CLng(Me.ContratoTextBox.Text)
                End If
                BUSCACLIENTES(0)
                Me.Bloque(True)
            End If

            If GloContrato > 0 Then
                zonas2 = Valida_Zonas()
            End If

            If zonas2 = True Then
                CheZonas.Visible = True
                CheZonas.Checked = True
                Labeltz.Visible = True
            Else
                CheZonas.Visible = False
                CheZonas.Checked = False
                Labeltz.Visible = False
            End If
            If GloContrato > 0 Then
                Me.Label26.Visible = True
                DamePerido(GloContrato)
                ChecaAdeudoDeMaterial(GloContrato)
            End If
        End If
    End Sub
    Private Sub DamePerido(ByVal contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_InformacionClienes", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = contrato
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                GloFechaPeriodoPagadoMes = reader(0).ToString()
                'Label3.Text = reader(1).ToString()
                'Label4.Text = reader(2).ToString()
                'Label5.Text = reader(3).ToString()
                Label26.Text = reader(4).ToString()
                GloFechaPeriodoFinal = reader(5).ToString()
                GloFechaPeridoPagado = Label26.Text
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub Adelantar_Pago()
        DAMETIPOSCLIENTEDAME()
        GloBnd = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'MsgBox(gloClv_Session & "," & gloClv_Servicio & "," & gloClv_llave & "," & gloClv_UnicaNet & "," & gloClave & "," & IdSistema & "," & GloAdelantados & "," & 0 & "," & Me.CLV_TIPOCLIENTELabel1.Text & "," & bndtodos & "," & Me.ContratoTextBox.Text & "," & BndError & "," & Msg)
        Me.PagosAdelantadosTableAdapter.Connection = CON
        Me.PagosAdelantadosTableAdapter.Fill(Me.NewsoftvDataSet.PagosAdelantados, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(gloClv_Servicio, Long)), New System.Nullable(Of Long)(CType(gloClv_llave, Long)), New System.Nullable(Of Long)(CType(gloClv_UnicaNet, Long)), New System.Nullable(Of Long)(CType(gloClave, Long)), IdSistema, New System.Nullable(Of Integer)(CType(GloAdelantados, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, 1, Me.ContratoTextBox.Text, BndError, Msg)
        CON.Dispose()
        CON.Close()
        Dim CON10 As New SqlConnection(MiConexion)
        CON10.Open()
        Me.DameDetalleTableAdapter.Connection = CON10
        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
        'GUARDE 
        Me.SumaDetalleTableAdapter.Connection = CON10
        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)
        CON10.Dispose()
        CON10.Close()
        Me.Clv_Session.Text = 0
        Me.Clv_Session.Text = gloClv_Session
        If IsNumeric(BndError) = False Then BndError = 1
        If BndError = 1 Then
            Me.LABEL19.Text = Msg
            Me.Panel5.Visible = True
            Me.Bloque(False)
        ElseIf BndError = 2 Then
            MsgBox(Msg)
        Else
            Me.Bloque(True)
        End If

    End Sub


    Private Sub FrmFacturaci�n_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim valida As Integer = 0
        'Dim MSG As String
        If Glocontratosel > 0 Then
            GloContrato = Glocontratosel
        End If


        Dim bndtodos As Integer = 0
        If GloContrato > 0 Then
            Me.Label26.Visible = True
            DamePerido(GloContrato)

        Else
            Me.Label26.Visible = False
        End If

        'Dim coneLidia As New SqlClient.SqlConnection(MiConexion)
        'Dim Cmd As New SqlClient.SqlCommand
        Try


            If ContratoTextBox.Tag = "1" Then
                ContratoTextBox.Tag = "0"
                Dim CON3 As New SqlConnection(MiConexion)
                CON3.Open()
                Me.DAMETOTALSumaDetalleTableAdapter.Connection = CON3
                Me.DAMETOTALSumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet1.DAMETOTALSumaDetalle, Me.Clv_Session.Text, 0, GLOIMPTOTAL)
                CON3.Dispose()
                CON3.Close()
                GLOSIPAGO = 0
                eBotonGuardar = True


                CHECADevolucionAparatosCliente(Clv_Session.Text, ContratoTextBox.Text, True)
                If eMsj.Length > 0 Then
                    If MessageBox.Show(eMsj, "�Atenci�n!", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                        eClv_Session = Clv_Session.Text
                        FrmDevolucionAparatosCliente.ShowDialog()
                    End If
                End If

                If VALIDABonificacion(Clv_Session.Text, GloTipoUsuario) = True Then
                    BonificacionCajera = False
                    locband_pant = 3
                    FrmSupervisorBonificacion.ShowDialog()
                    If BonificacionCajera = False Then Exit Sub
                End If

                FrmPago.Show()
            ElseIf ContratoTextBox.Tag = "2" Then
                ContratoTextBox.Tag = "0"
                GLOSIPAGO = 0
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)
            End If

            If bnd2pardep1 = True Then
                bnd2pardep1 = False
                Dim CON20 As New SqlConnection(MiConexion)
                CON20.Open()
                Me.DameDetalleTableAdapter.Connection = CON20
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, locclvsessionpardep, 0)
                '  , Loccontratopardep
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON20
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, locclvsessionpardep, False, 0)

                'RecalculaTotales_FacturacionNormal()

                CON20.Dispose()
                CON20.Close()
            End If


            If Glocontratosel > 0 Then
                Me.ContratoTextBox.Text = 0
                Me.ContratoTextBox.Text = Glocontratosel
                GloContrato = Glocontratosel
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                Me.BUSCACLIENTES(0)
            End If
            If GloBnd = True Then
                GloBnd = False
                If Glo_Apli_Pnt_Ade = True Then
                    bndtodos = 1
                    Glo_Apli_Pnt_Ade = False
                End If
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                'MsgBox(gloClv_Session & "," & gloClv_Servicio & "," & gloClv_llave & "," & gloClv_UnicaNet & "," & gloClave & "," & IdSistema & "," & GloAdelantados & "," & 0 & "," & Me.CLV_TIPOCLIENTELabel1.Text & "," & bndtodos & "," & Me.ContratoTextBox.Text & "," & BndError & "," & Msg)
                Me.PagosAdelantadosTableAdapter.Connection = CON
                Me.PagosAdelantadosTableAdapter.Fill(Me.NewsoftvDataSet.PagosAdelantados, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(gloClv_Servicio, Long)), New System.Nullable(Of Long)(CType(gloClv_llave, Long)), New System.Nullable(Of Long)(CType(gloClv_UnicaNet, Long)), New System.Nullable(Of Long)(CType(gloClave, Long)), IdSistema, New System.Nullable(Of Integer)(CType(GloAdelantados, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, bndtodos, Me.ContratoTextBox.Text, BndError, Msg)
                CON.Dispose()
                CON.Close()
                Dim CON10 As New SqlConnection(MiConexion)
                CON10.Open()
                Me.DameDetalleTableAdapter.Connection = CON10
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON10
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)
                CON10.Dispose()
                CON10.Close()
                Me.Clv_Session.Text = 0
                Me.Clv_Session.Text = gloClv_Session
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If
            If GloBndExt = True Then
                GloBndExt = False
                If GloClv_Txt = "CEXTV" Then
                    gloClv_Session = Me.Clv_Session.Text
                    Dim Error_1 As Long = 0
                    Dim CON8 As New SqlConnection(MiConexion)
                    CON8.Open()
                    Me.DimesiahiConexTableAdapter.Connection = CON8
                    Me.DimesiahiConexTableAdapter.Fill(Me.DataSetEdgar.DimesiahiConex, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), Error_1)
                    If Error_1 = 0 Then
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON8
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(1, Long)), IdSistema, New System.Nullable(Of Integer)(CType(0, Integer)), New System.Nullable(Of Integer)(CType(GloExt, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                        bitsist(GloUsuario, LiContrato, GloSistema, Me.Name, "Agregar Servicios", "Se Agrego un Servicio Adicional", "Servicio Agregado: " + CStr(GloClv_Txt) + " Tv Adicionales: " + CStr(GloExt), LocClv_Ciudad)
                    Else
                        MsgBox("Primero cobre la contrataci�n tvs. adicional que ya esta en la lista")
                    End If
                    CON8.Dispose()
                    CON8.Close()
                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = gloClv_Session
                    Dim CON19 As New SqlConnection(MiConexion)
                    CON19.Open()
                    Me.DameDetalleTableAdapter.Connection = CON19
                    Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
                    Me.SumaDetalleTableAdapter.Connection = CON19
                    Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)

                    CON19.Dispose()
                    CON19.Close()
                Else
                    gloClv_Session = Me.Clv_Session.Text
                    Dim CON9 As New SqlConnection(MiConexion)
                    CON9.Open()
                    If eBndPPE = True Then
                        eBndPPE = False
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Connection = CON9
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Fill(Me.DataSetEdgar.AgregarServicioAdicionales_PPE, Me.Clv_Session.Text, eClv_Progra, eClv_Txt)
                    Else
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON9
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(0, Long)), IdSistema, New System.Nullable(Of Integer)(CType(1, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                    End If

                    Me.DameDetalleTableAdapter.Connection = CON9
                    Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
                    Me.SumaDetalleTableAdapter.Connection = CON9
                    Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)

                    CON9.Dispose()
                    CON9.Close()

                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = gloClv_Session
                End If
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If
            If GloBonif = 1 Then
                GloBonif = 0
                Dim CON10 As New SqlConnection(MiConexion)
                CON10.Open()
                Me.DameDetalleTableAdapter.Connection = CON10
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON10
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)
                CON10.Dispose()
                CON10.Close()
            End If

            'If bndcontt = True Then
            '    bndcontt = False
            '    ConfigureCrystalReportsContratoTomatlan("TO")
            '    valida = MsgBox("Voltee la hoja Para Continuar la Impresi�n", MsgBoxStyle.YesNo, "Pausa")
            '    If valida = 6 Then
            '        ConfigureCrystalReportsContratoTomatlan2("TO")
            '    ElseIf valida = 7 Then
            '        MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
            '    End If
            '    Me.ContratoTextBox.Text = 0
            'End If

            If GLOSIPAGO = 1 Then 'SI YA PAGO PASA Y SI NO AL CHORIZO
                GLOSIPAGO = 0

                If IsNumeric(GLOTOTALEFECTIVO) = False Then GLOTOTALEFECTIVO = 0
                If IsNumeric(GLOCAMBIO) = False Then GLOCAMBIO = 0


                'CON11.Open()
                'Me.GrabaFacturasTableAdapter.Connection = CON11
                'Me.GrabaFacturasTableAdapter.Fill(Me.NewsoftvDataSet.GrabaFacturas, Me.ContratoTextBox.Text, Me.Clv_Session.Text, GloUsuario, GloSucursal, GloCaja, GloTipo, Loc_Serie, Loc_Folio, Loc_Clv_Vendedor, BndError, Msg, GloClv_Factura)
                ', , , , , 
                ''Inicia
                Try


                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "GrabaFacturas_2"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Me.ContratoTextBox.Text
                        .Parameters.Add(prm)

                        Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                        prm1.Direction = ParameterDirection.Input
                        prm1.Value = Me.Clv_Session.Text
                        .Parameters.Add(prm1)

                        Dim prm2 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
                        prm2.Direction = ParameterDirection.Input
                        prm2.Value = GloUsuario
                        .Parameters.Add(prm2)

                        Dim prm3 As New SqlParameter("@Sucursal", SqlDbType.Int)
                        prm3.Direction = ParameterDirection.Input
                        prm3.Value = GloSucursal
                        .Parameters.Add(prm3)

                        Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
                        prm4.Direction = ParameterDirection.Input
                        prm4.Value = GloCaja
                        .Parameters.Add(prm4)

                        Dim prm5 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
                        prm5.Direction = ParameterDirection.Input
                        If Me.SplitContainer1.Panel1Collapsed = False Then
                            GloTipo = "V"
                            prm5.Value = "V"
                        Else
                            GloTipo = "C"
                            prm5.Value = "C"
                        End If

                        .Parameters.Add(prm5)

                        Dim prm6 As New SqlParameter("@Serie_V", SqlDbType.VarChar, 5)
                        prm6.Direction = ParameterDirection.Input
                        If Len(Loc_Serie) = 0 Then
                            Loc_Serie = ""
                        End If
                        prm6.Value = Loc_Serie
                        .Parameters.Add(prm6)


                        Dim prm7 As New SqlParameter("@Folio_V", SqlDbType.BigInt)
                        prm7.Direction = ParameterDirection.Input
                        If IsNumeric(Loc_Folio) = False Then
                            Loc_Folio = 0
                        End If
                        prm7.Value = Loc_Folio
                        .Parameters.Add(prm7)

                        Dim prm8 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                        prm8.Direction = ParameterDirection.Input
                        prm8.Value = Loc_Clv_Vendedor
                        .Parameters.Add(prm8)

                        Dim prm9 As New SqlParameter("@BndError", SqlDbType.Int)
                        prm9.Direction = ParameterDirection.Input
                        prm9.Value = BndError
                        .Parameters.Add(prm9)

                        Dim prm10 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
                        prm10.Direction = ParameterDirection.Input
                        prm10.Value = Msg
                        .Parameters.Add(prm10)

                        Dim prm17 As New SqlParameter("@Clv_FacturaSalida", SqlDbType.BigInt)
                        prm17.Direction = ParameterDirection.Output
                        prm17.Value = 0
                        .Parameters.Add(prm17)
                        '--1                    
                        Dim prm18 As New SqlParameter("@1Tipo", SqlDbType.Int)
                        prm18.Direction = ParameterDirection.Input
                        If IsNumeric(GloTipoTarjeta) = True Then
                            prm18.Value = GloTipoTarjeta
                        Else
                            prm18.Value = 0
                        End If
                        .Parameters.Add(prm18)

                        Dim prm19 As New SqlParameter("@1Monto", SqlDbType.Money)
                        prm19.Direction = ParameterDirection.Input
                        If IsNumeric(GLOTARJETA) = True Then
                            prm19.Value = GLOTARJETA
                        Else
                            prm19.Value = 0
                        End If
                        .Parameters.Add(prm19)
                        '--2
                        Dim prm22 As New SqlParameter("@2GLOEFECTIVO", SqlDbType.Money)
                        prm22.Direction = ParameterDirection.Input
                        If IsNumeric(GLOEFECTIVO) = True Then
                            prm22.Value = GLOEFECTIVO
                        Else
                            prm22.Value = 0
                        End If
                        .Parameters.Add(prm22)

                        Dim prm23 As New SqlParameter("@2GLOCHEQUE", SqlDbType.Money)
                        prm23.Direction = ParameterDirection.Input
                        If IsNumeric(GLOCHEQUE) = True Then
                            prm23.Value = GLOCHEQUE
                        Else
                            prm23.Value = 0
                        End If

                        .Parameters.Add(prm23)

                        Dim prm24 As New SqlParameter("@2GLOCLV_BANCOCHEQUE", SqlDbType.Int)
                        prm24.Direction = ParameterDirection.Input
                        If IsNumeric(GLOCLV_BANCOCHEQUE) = True Then
                            prm24.Value = GLOCLV_BANCOCHEQUE
                        Else
                            prm24.Value = 0
                        End If
                        .Parameters.Add(prm24)

                        Dim prm25 As New SqlParameter("@2NUMEROCHEQUE", SqlDbType.VarChar, 50)
                        prm25.Direction = ParameterDirection.Input
                        If Len(NUMEROCHEQUE) > 0 Then
                            prm25.Value = NUMEROCHEQUE
                        Else
                            prm25.Value = ""
                        End If
                        .Parameters.Add(prm25)

                        Dim prm26 As New SqlParameter("@2GLOTARJETA", SqlDbType.Money)
                        prm26.Direction = ParameterDirection.Input
                        If IsNumeric(GLOTARJETA) = True Then
                            prm26.Value = GLOTARJETA
                        Else
                            prm26.Value = 0
                        End If
                        .Parameters.Add(prm26)

                        Dim prm27 As New SqlParameter("@2GLOCLV_BANCOTARJETA", SqlDbType.Int)
                        prm27.Direction = ParameterDirection.Input
                        If IsNumeric(GLOCLV_BANCOTARJETA) = True Then
                            prm27.Value = GLOCLV_BANCOTARJETA
                        Else
                            prm27.Value = 0
                        End If
                        .Parameters.Add(prm27)

                        Dim prm28 As New SqlParameter("@2NUMEROTARJETA", SqlDbType.VarChar, 50)
                        prm28.Direction = ParameterDirection.Input
                        If Len(NUMEROTARJETA) > 0 Then
                            prm28.Value = NUMEROTARJETA
                        Else
                            prm28.Value = 0
                        End If
                        .Parameters.Add(prm28)

                        Dim prm29 As New SqlParameter("@2TARJETAAUTORIZACION", SqlDbType.VarChar, 50)
                        prm29.Direction = ParameterDirection.Input
                        prm29.Value = TARJETAAUTORIZACION
                        .Parameters.Add(prm29)
                        '--3
                        Dim prm32 As New SqlParameter("@3CLV_Nota", SqlDbType.BigInt)
                        prm32.Direction = ParameterDirection.Input
                        prm32.Value = GLOCLV_NOTA
                        .Parameters.Add(prm32)

                        Dim prm33 As New SqlParameter("@3GLONOTA", SqlDbType.Money)
                        prm33.Direction = ParameterDirection.Input
                        prm33.Value = GLONOTA
                        .Parameters.Add(prm33)


                        Dim i As Integer = comando.ExecuteNonQuery()
                        GloClv_Factura = prm17.Value

                    End With
                    CON.Close()
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                If IsNumeric(GLOEFECTIVO) = False Then GLOEFECTIVO = 0
                If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
                If IsNumeric(GLOCLV_BANCOCHEQUE) = False Then GLOCLV_BANCOCHEQUE = 0
                If Len(NUMEROCHEQUE) = 0 Then NUMEROCHEQUE = ""
                If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
                If IsNumeric(GLOCLV_BANCOTARJETA) = False Then GLOCLV_BANCOTARJETA = 0
                If Len(NUMEROTARJETA) = 0 Then NUMEROTARJETA = ""
                If Len(TARJETAAUTORIZACION) = 0 Then TARJETAAUTORIZACION = ""

                If GLOTOTALEFECTIVO > 0 Then
                    NUEPago_En_EfectivoDet(GloClv_Factura, GLOTOTALEFECTIVO, GLOCAMBIO)
                End If

                'FacturaFiscalCFD---------------------------------------------------------------------
                'facturaFiscalCFD = False
                'facturaFiscalCFD = ChecaSiEsFacturaFiscal("N", GloClv_Factura)
                'If facturaFiscalCFD = True Then
                '    DameSerieFolio(0, GloClv_Factura)
                '    GeneraFacturaCFD("N", GloClv_Factura, eSerie, eFolio, ContratoTextBox.Text, "")
                '    MsgBox("Se gener� una Factura Fiscal Electr�nica con el nombre de " + eSerie + eFolio.ToString() + ".ff")
                'End If
                '-------------------------------------------------------------------------------------

                'Dim CON2 As New SqlConnection(MiConexion)

                'Guarda_Tipo_Tarjeta(GloClv_Factura, GloTipoTarjeta, GLOTARJETA)

                'CON.Open()

                'Dim comando2 As SqlClient.SqlCommand
                'comando2 = New SqlClient.SqlCommand
                'With comando2
                '    .Connection = CON
                '    .CommandText = "GUARDATIPOPAGO "
                '    .CommandType = CommandType.StoredProcedure
                '    .CommandTimeout = 0

                '    Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                '    prm.Direction = ParameterDirection.Input
                '    prm.Value = GloClv_Factura
                '    .Parameters.Add(prm)

                '    Dim prm2 As New SqlParameter("@GLOEFECTIVO", SqlDbType.Money)
                '    prm2.Direction = ParameterDirection.Input
                '    prm2.Value = GLOEFECTIVO
                '    .Parameters.Add(prm2)

                '    Dim prm3 As New SqlParameter("@GLOCHEQUE", SqlDbType.Money)
                '    prm3.Direction = ParameterDirection.Input
                '    prm3.Value = GLOCHEQUE
                '    .Parameters.Add(prm3)

                '    Dim prm4 As New SqlParameter("@GLOCLV_BANCOCHEQUE", SqlDbType.Int)
                '    prm4.Direction = ParameterDirection.Input
                '    prm4.Value = GLOCLV_BANCOCHEQUE
                '    .Parameters.Add(prm4)

                '    Dim prm5 As New SqlParameter("@NUMEROCHEQUE", SqlDbType.VarChar)
                '    prm5.Direction = ParameterDirection.Input
                '    prm5.Value = NUMEROCHEQUE
                '    .Parameters.Add(prm5)

                '    Dim prm6 As New SqlParameter("@GLOTARJETA", SqlDbType.Money)
                '    prm6.Direction = ParameterDirection.Input
                '    prm6.Value = GLOTARJETA
                '    .Parameters.Add(prm6)

                '    Dim prm7 As New SqlParameter("@GLOCLV_BANCOTARJETA", SqlDbType.Int)
                '    prm7.Direction = ParameterDirection.Input
                '    prm7.Value = GLOCLV_BANCOTARJETA
                '    .Parameters.Add(prm7)

                '    Dim prm8 As New SqlParameter("@NUMEROTARJETA", SqlDbType.VarChar)
                '    prm8.Direction = ParameterDirection.Input
                '    prm8.Value = NUMEROTARJETA
                '    .Parameters.Add(prm8)

                '    Dim prm9 As New SqlParameter("@TARJETAAUTORIZACION", SqlDbType.VarChar)
                '    prm9.Direction = ParameterDirection.Input
                '    prm9.Value = TARJETAAUTORIZACION
                '    .Parameters.Add(prm9)

                '    Dim j As Integer = comando2.ExecuteNonQuery()

                'End With
                'CON.Close()
                ''Inica Guarda Si el Tipo de Pago fue con una Nota de Credito
                'Dim CON3 As New SqlConnection(MiConexion)
                'If GLOCLV_NOTA > 0 And GloClv_Factura > 0 And GLONOTA > 0 Then
                '    CON.Open()

                '    Dim comando3 As SqlClient.SqlCommand
                '    comando3 = New SqlClient.SqlCommand
                '    With comando3
                '        .Connection = CON
                '        .CommandText = "GUARDATIPOPAGO_Nota_Credito "
                '        .CommandType = CommandType.StoredProcedure
                '        .CommandTimeout = 0

                '        Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                '        prm.Direction = ParameterDirection.Input
                '        prm.Value = GloClv_Factura
                '        .Parameters.Add(prm)

                '        Dim prm2 As New SqlParameter("@CLV_Nota", SqlDbType.BigInt)
                '        prm2.Direction = ParameterDirection.Input
                '        prm2.Value = GLOCLV_NOTA
                '        .Parameters.Add(prm2)

                '        Dim prm3 As New SqlParameter("@GLONOTA", SqlDbType.Money)
                '        prm3.Direction = ParameterDirection.Input
                '        prm3.Value = GLONOTA
                '        .Parameters.Add(prm3)

                '        Dim j As Integer = comando3.ExecuteNonQuery()

                '    End With
                '    CON.Close()
                'End If

                'Fin Guarda si el tipo de Pago es con Nota de Credito


                ''Termina
                'Me.GUARDATIPOPAGOTableAdapter.Connection = CON11
                'Me.GUARDATIPOPAGOTableAdapter.Fill(Me.NewsoftvDataSet1.GUARDATIPOPAGO, GloClv_Factura, GLOEFECTIVO, GLOCHEQUE, GLOCLV_BANCOCHEQUE, NUMEROCHEQUE, GLOTARJETA, GLOCLV_BANCOTARJETA, NUMEROTARJETA, TARJETAAUTORIZACION)


                Dim CON11 As New SqlConnection(MiConexion)
                CON11.Open()
                Me.Dime_ContratacionTableAdapter.Connection = CON11
                Me.Dime_ContratacionTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Contratacion, GloClv_Factura, res)

                'If eMotivoBonificacion.Length > 0 Then
                '    Me.GuardaMotivosBonificacionTableAdapter.Connection = CON11
                '    Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, GloClv_Factura, eMotivoBonificacion)
                'End If
                'graba si hay bonificacion
                If locBndBon1 = True Then
                    If GloClv_Factura > 0 Then
                        Me.Inserta_Bonificacion_SupervisorTableAdapter.Connection = CON11
                        Me.Inserta_Bonificacion_SupervisorTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Bonificacion_Supervisor, GloClv_Factura, LocSupBon)
                        bitsist(GloUsuario, Me.ContratoTextBox.Text, GloSistema, Me.Name, "Se Realizo Una Bonificaci�n", "", "Factura:" + CStr(GloClv_Factura), SubCiudad)
                    End If
                End If

                'CON11.Dispose()
                CON11.Close()
                'MsgBox(Msg)
                If LocImpresoraTickets = "" Then
                    MsgBox("No Se Ha Asignado Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                    GLOSIPAGO = 0
                    Me.ContratoTextBox.Text = 0
                    GloContrato = 0
                    Glocontratosel = 0
                    Me.Clv_Session.Text = 0
                    BUSCACLIENTES(0)
                Else
                    ConfigureCrystalReports(GloClv_Factura)
                End If

                Mana_ImprimirOrdenes(GloClv_Factura)
                If GloTipo = "V" Then
                    'Me.Dame_UltimoFolio()
                    LenaFolios()
                End If

                'FrmImprimir.Show()
                If res = 1 Then
                    If LocNomImpresora_Contratos = "" Then
                        MsgBox("No se ha asignado una Impresora de Contratos a esta Sucursal", MsgBoxStyle.Information)
                        Me.ContratoTextBox.Text = 0
                        GloContrato = 0
                        Glocontratosel = 0
                        Me.Clv_Session.Text = 0
                        BUSCACLIENTES(0)
                    Else
                        If IdSistema = "TO" Then
                            '' FrmHorasInst.Show()
                            horaini = "0"
                            horafin = "0"
                            bndcontt = True
                            If bndcontt = True Then
                                bndcontt = False
                                ConfigureCrystalReportsContratoTomatlan("TO")
                                valida = MsgBox("Voltee la hoja Para Continuar la Impresi�n", MsgBoxStyle.YesNo, "Pausa")
                                If valida = 6 Then
                                    ConfigureCrystalReportsContratoTomatlan2("TO")
                                ElseIf valida = 7 Then
                                    MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
                                End If
                                Me.ContratoTextBox.Text = 0
                                GloContrato = 0
                                Glocontratosel = 0
                                Me.Clv_Session.Text = 0
                                BUSCACLIENTES(0)
                            End If
                        End If
                    End If
                End If
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)
                If GloTipo = "V" Then
                    LenaFolios()
                End If
                'Proceso para Checar el Monto de la Cajera ===========
                'coneLidia.Open()
                'With Cmd
                '    .CommandText = "Verifica_Monto"
                '    .CommandTimeout = 0
                '    .CommandType = CommandType.StoredProcedure
                '    .Connection = coneLidia
                '    Dim prm As New SqlParameter("@Cajera", SqlDbType.VarChar)
                '    Dim prm2 As New SqlParameter("@Alerta", SqlDbType.Int)
                '    prm.Direction = ParameterDirection.Input
                '    prm2.Direction = ParameterDirection.Output
                '    prm.Value = GloUsuario
                '    prm2.Value = 0
                '    .Parameters.Add(prm)
                '    .Parameters.Add(prm2)
                '    Dim i As Integer = Cmd.ExecuteNonQuery
                '    Verifica = prm2.Value
                'End With
                'If Verifica = 5 Then
                '    BndAlerta = True
                'Else
                '    BndAlerta = False
                'End If
                'Me.Enabled = True
            ElseIf GLOSIPAGO = 2 Then 'CANCELO EL PAGO
                GLOSIPAGO = 0
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)
                'Me.Enabled = True
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            GLOSIPAGO = 0
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)
        End Try
        If eBotonGuardar = True Then
            Bloque(False)
        End If

        If GloContrato > 0 Then
            zonas2 = Valida_Zonas()
        End If


        If zonas2 = True Then
            CheZonas.Visible = True
            CheZonas.Checked = True
            Labeltz.Visible = True
        Else
            CheZonas.Visible = False
            CheZonas.Checked = False
            Labeltz.Visible = False
        End If

    End Sub

    Private Sub NUEPago_En_EfectivoDet(ByVal Clv_Factura As Integer, ByVal Efectivo As Decimal, ByVal Cambio As Decimal)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.Int, Clv_Factura)
        BaseII.CreateMyParameter("@Efectivo", SqlDbType.Decimal, Efectivo)
        BaseII.CreateMyParameter("@Cambio", SqlDbType.Decimal, Cambio)
        BaseII.Inserta("NUEPago_En_EfectivoDet")
    End Sub

    Private Function VALIDABonificacion(ByVal CLV_SESSION As Integer, ByVal CLV_TIPOUSUARIO As Integer) As Boolean
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@CLV_TIPOUSUARIO", SqlDbType.Int, CLV_TIPOUSUARIO)
        BaseII.CreateMyParameter("@AUTORIZACION", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("VALIDABonificacion")
        Return Boolean.Parse(BaseII.dicoPar("@AUTORIZACION").ToString)
    End Function

    Private Sub ConfigureCrystalReportsContratoTomatlan(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            'Dim horaini As String = nothing
            'Dim horafin As String = nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ContratoTomatlan.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)


            'horaini = InputBox("Apartir de ", "Captura Hora")
            'horafin = InputBox("Capture la hora de la Instalaci�n Final", "Captura Hora")



            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            'If horaini = "" Then horaini = "0"
            'If horafin = "" Then horafin = "0"

            customersByCityReport.DataDefinition.FormulaFields("horaini").Text = "'" & horaini & "'"
            customersByCityReport.DataDefinition.FormulaFields("horafin").Text = "'" & horafin & "'"


            ' customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Hora_insTableAdapter.Connection = CON
            Me.Hora_insTableAdapter.Fill(Me.NewsoftvDataSet2.Hora_ins, horaini, horafin, GloContrato)
            Me.Inserta_Comentario2TableAdapter.Connection = CON
            Me.Inserta_Comentario2TableAdapter.Fill(Me.NewsoftvDataSet2.Inserta_Comentario2, GloContrato)
            CON.Dispose()
            CON.Close()
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub ConfigureCrystalReportsContratoTomatlan2(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\TomatlanAtras.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)

            'customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub FrmFacturaci�n_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsNumeric(Me.Clv_Session.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorraClv_SessionTableAdapter.Connection = CON
            Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
            CON.Dispose()
            CON.Close()
        End If
    End Sub

    Private Sub FrmFAC_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.HandleDestroyed

    End Sub



    Private Sub FrmFac_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEdgar.MUESTRAVENDEDORES_2' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.Ultimo_SERIEYFOLIO' Puede moverla o quitarla seg�n sea necesario.
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.MuestraVendedores' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.MuestraPromotores' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.DameDatosGenerales' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DamedatosUsuarioTableAdapter.Connection = CON
        Me.DamedatosUsuarioTableAdapter.Fill(Me.NewsoftvDataSet.DamedatosUsuario, GloUsuario)
        Me.DAMENOMBRESUCURSALTableAdapter.Connection = CON
        Me.DAMENOMBRESUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMENOMBRESUCURSAL, GloSucursal)
        Me.DameDatosGeneralesTableAdapter.Connection = CON
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)
        CON.Dispose()
        CON.Close()
        Me.REDLabel25.Visible = False
        Me.LblNomCaja.Text = GlonOMCaja
        Me.LblVersion.Text = My.Application.Info.Version.ToString
        If GloTipo = "V" Then
            Me.SplitContainer1.Panel1Collapsed = False
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            Me.MUESTRAVENDEDORES_2TableAdapter.Connection = CON1
            Me.MUESTRAVENDEDORES_2TableAdapter.Fill(Me.DataSetEdgar.MUESTRAVENDEDORES_2)
            CON1.Dispose()
            CON1.Close()
            Me.ComboBox1.Text = ""
            Me.ComboBox1.SelectedValue = 0
        Else
            Me.SplitContainer1.Panel1Collapsed = True
            Me.ComboBox1.TabStop = False
            Me.ComboBox2.TabStop = False
        End If
        Label22.Visible = False
        Fecha_Venta.Visible = False
        If IdSistema = "SA" Then
            Me.Button12.Visible = True
            'Me.ButtonBajaTemporal.Visible = False
            If GloTipo = "V" Then
                Label22.Visible = True
                Fecha_Venta.Visible = True
            End If
        End If
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON2
        Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.NewsoftvDataSet2.Selecciona_Impresora_Sucursal, GloSucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)
        CON2.Dispose()
        CON2.Close()
        Bloque(False)


        BtnPagMaterial.Enabled = False
        If GloHabilitadosNET = False Then
            SOLOINTERNETCheckBox.Visible = False

            'SOLOINTERNETLabel1.visible = False
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
        If ContratoTextBox.Text > 0 Then
            DamePerido(LiContrato)
        Else
            Me.Label26.Visible = False
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Glocontratosel = 0
        GloContrato = 0
        eBotonGuardar = False
        'If IdSistema = "VA" Or IdSistema = "LO" Then
        '    FrmSelCliente2.Show()
        'ElseIf IdSistema <> "VA" And IdSistema <> "LO" Then
        FrmSelCliente.Show()
        'End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If locbndborracabiosdep = True Then
        '    'locbndborracabiosdep = False
        '    borracambiosdeposito(locclvsessionpardep, Loccontratopardep)
        'End If
        Me.Close()
    End Sub
    Private Sub YaTengoClv_Session(ByVal mclv_session As Long)
        If IsNumeric(mclv_session) = True Then
            Me.Panel5.Visible = False
            locBndBon1 = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, mclv_session, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, mclv_session, False, 0)
            CON.Dispose()
            CON.Close()
            ''CREAARBOL1()
        End If
    End Sub


    'Private Sub Clv_Session_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_Session.TextChanged
    '    If IsNumeric(Me.Clv_Session.Text) = True Then
    '        Me.Panel5.Visible = False
    '        locBndBon1 = False
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Me.DameDetalleTableAdapter.Connection = CON
    '        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
    '        Me.SumaDetalleTableAdapter.Connection = CON
    '        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
    '        CON.Dispose()
    '        CON.Close()
    '        ''CREAARBOL1()
    '    End If
    'End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Try

            BtnPagMaterial.Enabled = False
        If e.ColumnIndex = 7 Then
            If SiPagos = 1 Then
                MsgBox("No se puede adelantar pagos con la promoci�n que se le esta aplicando")
                Exit Sub
            End If
            If DataGridView1.SelectedCells(7).Value = "Adelantar Pagos" Then
                If IsNumeric(DataGridView1.SelectedCells(0).Value) = True And IsNumeric(DataGridView1.SelectedCells(1).Value) = True And IsNumeric(DataGridView1.SelectedCells(2).Value) = True And IsNumeric(Me.ContratoTextBox.Text) = True Then
                    gloClv_Session = DataGridView1.SelectedCells(0).Value
                    gloClv_Servicio = DataGridView1.SelectedCells(1).Value
                    gloClv_llave = DataGridView1.SelectedCells(2).Value
                    gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                    gloClave = DataGridView1.SelectedCells(4).Value
                    Dim ERROR_1 As Integer = 0
                    Dim MSGERROR_1 As String = Nothing
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Connection = CON
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Fill(Me.DataSetEdgar.Pregunta_Si_Puedo_Adelantar, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), ERROR_1, MSGERROR_1)
                    CON.Dispose()
                    CON.Close()
                    If ERROR_1 = 0 Then

                        'Adelantar Pagos para Clientes con tipo de cobro Pagos Diferidos
                        ChecaAdelantarPagosDif(ContratoTextBox.Text)
                        If eRes = 1 Then
                            Dim r As Integer
                            r = MsgBox(eMsj, MsgBoxStyle.YesNo)
                            If r = 6 Then My.Forms.FrmPagosAdelantados.Show()
                            Exit Sub
                        ElseIf eRes = 2 Then
                            MsgBox(eMsj, MsgBoxStyle.Exclamation)
                            Exit Sub
                        End If

                        My.Forms.FrmPagosAdelantados.Show()

                    ElseIf ERROR_1 = 2 Then
                        MsgBox(MSGERROR_1)
                    End If
                End If
            ElseIf DataGridView1.SelectedCells(7).Value.ToString.Contains("Ext. Adicionales") Then
                GloClv_Txt = "CEXTV"
                Dim fmrexttv As New FrmExtecionesTv
                fmrexttv.ShowDialog()


            ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                'FrmDetCobroDesc.Show()
                Dim Frmcobro As New FrmDetCobroDescMate
                    Frmcobro.ShowDialog()
                        BtnPagMaterial.Enabled = True

                    


                End If
           
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub clibloqueado()
        Me.Button7.Enabled = False
        Me.Button8.Enabled = False
        Me.Button2.Enabled = False
        Me.Button6.Enabled = False
        Me.Button5.Enabled = False
        ButtonBajaTemporal.Enabled = False
        Me.NOMBRELabel1.Text = ""
        Me.CALLELabel1.Text = ""
        Me.NUMEROLabel1.Text = ""
        Me.COLONIALabel1.Text = ""
        Me.CIUDADLabel1.Text = ""
    End Sub

    Private Sub Bloque(ByVal bnd As Boolean)
        Me.Button5.Enabled = bnd
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                Me.Button5.Enabled = True
            End If
        End If
        'Me.Button6.Enabled = bnd
        Me.Button7.Enabled = bnd
        Me.Button8.Enabled = bnd
        Me.Button2.Enabled = bnd
        Me.Button6.Enabled = bnd
        ButtonBajaTemporal.Enabled = bnd

        If Me.DataGridView1.RowCount = 0 Then
            Me.Button7.Enabled = False
            Me.Button6.Enabled = False
            Me.Button2.Enabled = False
            Me.Button8.Enabled = False
            Me.Button5.Enabled = False
            ButtonBajaTemporal.Enabled = False
        Else
            Me.Button7.Enabled = True
            Me.Button6.Enabled = True
            Me.Button2.Enabled = True
            Me.Button8.Enabled = True
            Me.Button5.Enabled = True
            ButtonBajaTemporal.Enabled = True
        End If


        'If Me.ContratoTextBox.Text.Length = 0 Then
        '    Me.ContratoTextBox.Text = 0
        'End If
1:
        'Dim CON2 As New SqlConnection(MiConexion)
        'CON2.Open()
        'Me.DameServicioAsignadoTableAdapter.Connection = CON2
        'Me.DameServicioAsignadoTableAdapter.Fill(Me.EricDataSet.DameServicioAsignado, GloContrato, eRes)
        'CON2.Dispose()
        'CON2.Close()
        'If eRes = 1 Then
        '    eRes = 0
        '    Me.ButtonBajaTemporal.Enabled = True
        'Else
        '    eRes = 0
        '    Me.ButtonBajaTemporal.Enabled = False
        'End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        gloClv_Session = Me.Clv_Session.Text
        eBotonGuardar = False
        FrmServicios.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.ContratoTextBox.Text = 0
        GloContrato = 0
        Glocontratosel = 0
        Me.Clv_Session.Text = 0
        BUSCACLIENTES(0)
        Bloque(False)
    End Sub

    Public Function DIME_SI_YA_GRABE_UNA_FACTURA(ByVal MiContrato As Long) As Integer
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()

        Try
            DIME_SI_YA_GRABE_UNA_FACTURA = 0
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "DIME_SI_YA_GRABE_UNA_FACTURA"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = MiContrato
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("@Valida", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = MiContrato
                .Parameters.Add(prm2)
                Dim ia As Integer = .ExecuteNonQuery()
                DIME_SI_YA_GRABE_UNA_FACTURA = prm2.Value
            End With
            CON100.Close()
        Catch ex As Exception
            DIME_SI_YA_GRABE_UNA_FACTURA = 0
            If CON100.State <> ConnectionState.Closed Then CON100.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function


    Private Sub BotonGrabar()
        Try
            If IsNumeric(Me.ContratoTextBox.Text) = True And IsNumeric(Me.Clv_Session.Text) = True Then
                If Me.ContratoTextBox.Text > 0 And Me.Clv_Session.Text > 0 Then
                    If GloTipo = "V" Then
                        If Len(Trim(Me.ComboBox2.Text)) = 0 Then
                            MsgBox("Seleccione una Serie de la lista ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.ComboBox1.SelectedValue) = False And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                            MsgBox("Seleccione un Vendedor de la lista ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.ComboBox3.SelectedValue) = False Then
                            MsgBox("Capture un Folio de la lista ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If uspChecaFolioUsado(CInt(Me.ComboBox1.SelectedValue), Me.ComboBox2.Text, CInt(Me.ComboBox3.Text)) > 0 Then
                            MsgBox("El Folio ya ha sido utilizado anteriormente", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        Loc_Serie = Me.ComboBox2.Text
                        Loc_Clv_Vendedor = Me.ComboBox1.SelectedValue
                        Loc_Folio = Me.ComboBox3.Text
                    End If
                    'Dim CON3 As New SqlConnection(MiConexion)
                    'CON3.Open()
                    'Me.DAMETOTALSumaDetalleTableAdapter.Connection = CON3
                    'Me.DAMETOTALSumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet1.DAMETOTALSumaDetalle, Me.Clv_Session.Text, 0, GLOIMPTOTAL)
                    'CON3.Dispose()
                    'CON3.Close()
                    'GLOSIPAGO = 0
                    'eBotonGuardar = True
                    'FrmPago.Show()
                    If Me.DIME_SI_YA_GRABE_UNA_FACTURA(Me.ContratoTextBox.Text) = 0 Then
                        Dim CON3 As New SqlConnection(MiConexion)
                        CON3.Open()
                        Me.DAMETOTALSumaDetalleTableAdapter.Connection = CON3
                        Me.DAMETOTALSumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet1.DAMETOTALSumaDetalle, Me.Clv_Session.Text, 0, GLOIMPTOTAL)
                        CON3.Dispose()
                        CON3.Close()
                        GLOSIPAGO = 0
                        eBotonGuardar = True

                        CHECADevolucionAparatosCliente(Clv_Session.Text, ContratoTextBox.Text, True)
                        If eMsj.Length > 0 Then
                            If MessageBox.Show(eMsj, "�Atenci�n!", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                                eClv_Session = Clv_Session.Text
                                FrmDevolucionAparatosCliente.ShowDialog()
                            End If
                        End If

                        'If VALIDABonificacion(Clv_Session.Text, GloTipoUsuario) = True Then
                        '    BonificacionCajera = False
                        '    locband_pant = 3
                        '    FrmSupervisorBonificacion.ShowDialog()
                        '    If BonificacionCajera = False Then Exit Sub
                        'End If

                        FrmPago.Show()
                    Else
                        Me.ContratoTextBox.Tag = "0"
                        DialogTiene_Facturas.Show()

                    End If
                    'LenaFolios()
                    'Me.Enabled = False
                Else
                    MsgBox("Seleccione un Cliente para Facturar ", MsgBoxStyle.Information)
                    eBotonGuardar = False
                End If
                'LenaFolios()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show("No hay Servicios o Conceptos que Facturar")
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)
            eBotonGuardar = True
        End Try
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BUSCACLIENTES(0)
    End Sub

    Private Sub Clv_Vendedor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If IsNumeric(Me.Clv_Vendedor.SelectedValue) = True And Len(Trim(Me.Clv_Vendedor.Text)) > 0 Then
        ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, Me.Clv_Vendedor.SelectedValue)
        ' Me.ComboBox2.Text = ""
        ' Else
        ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, 0)
        'End If
    End Sub



    Private Sub ComboBox2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.ComboBox2.SelectedValue) = True And Len(Trim(ComboBox2.Text)) Then
            'Dame_UltimoFolio()
            If GloTipo = "V" Then
                LenaFolios()
            End If
        Else
            Me.ComboBox3.Text = ""
        End If
    End Sub

    'Private Sub Dame_UltimoFolio()
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Me.FolioTextBox.Text = 0
    '        Me.DAMEUltimo_FOLIOTableAdapter.Connection = CON
    '        Me.DAMEUltimo_FOLIOTableAdapter.Fill(Me.DataSetEdgar.DAMEUltimo_FOLIO, Me.ComboBox1.SelectedValue, Me.ComboBox2.Text, Me.FolioTextBox.Text)
    '        CON.Dispose()
    '        CON.Close()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub
    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
            Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, Me.ComboBox1.SelectedValue)
            Me.ComboBox2.Text = ""
        Else
            Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, 0)
            Me.ComboBox2.Text = ""
        End If
        CON.Dispose()
        CON.Close()
    End Sub

    Private Sub ComboBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox2.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        'Me.FolioTextBox.Text = ""
        Me.ComboBox3.Text = ""
        If Len(Trim(ComboBox2.Text)) Then
            'Dame_UltimoFolio()
            If GloTipo = "V" Then
                LenaFolios()
            End If
        End If
    End Sub

    Private Sub ComboBox2_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.TextChanged
        'Me.FolioTextBox.Text = ""
        Me.ComboBox3.Text = ""
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'If IsNumeric(GloContrato) = True And GloContrato > 0 Then
        If IsNumeric(Me.ContratoTextBox.Text) = True And CInt(Me.ContratoTextBox.Text) > 0 Then
            'GloOpFacturas = 3
            'eBotonGuardar = False
            'BrwFacturas_Cancelar.Show()
            FrmSeleccionaTipo.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim LOCCLAVE As Integer = 0
            'MsgBox(DataGridView1.SelectedCells(29).Value)
            If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
                If IsNumeric(DataGridView1.SelectedCells(4).Value) = False Then LOCCLAVE = 0 Else LOCCLAVE = DataGridView1.SelectedCells(4).Value
                If (LOCCLAVE = 1 Or LOCCLAVE = 3) And DataGridView1.SelectedCells(7).Value <> "Ext. Adicionales" Then
                    If LOCCLAVE = 1 Then MsgBox("No se puede quitar la Contrataci�n", MsgBoxStyle.Information)
                    If LOCCLAVE = 2 Then MsgBox("No se puede quitar la Reconexi�n", MsgBoxStyle.Information)
                    Exit Sub
                End If
                '--MsgBox(DataGridView1.SelectedCells(0).Value & "," & DataGridView1.SelectedCells(29).Value & "," & IdSistema & "," & Me.CLV_TIPOCLIENTELabel1.Text)
                Me.BORCAMDOCFAC_QUITATableAdapter.Connection = CON
                Me.BORCAMDOCFAC_QUITATableAdapter.Fill(Me.NewsoftvDataSet2.BORCAMDOCFAC_QUITA, gloClv_Session)
                'MsgBox(DataGridView1.SelectedCells(0).Value & "," & DataGridView1.SelectedCells(29).Value & "," & IdSistema & "," & Me.CLV_TIPOCLIENTELabel1.Text & "," & BndError & "," & Msg)
                'Me.QUITARDELDETALLETableAdapter.Connection = CON
                'Me.QUITARDELDETALLETableAdapter.Fill(Me.NewsoftvDataSet1.QUITARDELDETALLE, DataGridView1.SelectedCells(0).Value, DataGridView1.SelectedCells(29).Value, IdSistema, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                QUITARDELDETALLE(DataGridView1.SelectedCells(0).Value, DataGridView1.SelectedCells(29).Value, IdSistema, Me.CLV_TIPOCLIENTELabel1.Text, GloContrato)

                bitsist(GloCajera, LiContrato, GloSistema, Me.Name, "", "Se quito del detalle", "Concepto: " + CStr(DataGridView1.SelectedCells(7).Value), LocClv_Ciudad)
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
                Me.DameDetalleTableAdapter.Connection = CON
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
                Me.SumaDetalleTableAdapter.Connection = CON
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
                ''gloClv_Session = DataGridView1.SelectedCells(0).Value        
                ''gloClave = DataGridView1.SelectedCells(4).Value
            End If
            CON.Dispose()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        eBotonGuardar = False

        'If Me.DataGridView1.RowCount = 0 Then
        ' Me.Button7.Enabled = False
        'Me.Button6.Enabled = False
        'Me.Button2.Enabled = False
        'End If
    End Sub

    Private Sub DAMETIPOSCLIENTEDAME()
        Try
            Me.CLV_TIPOCLIENTELabel1.Text = ""
            Me.DESCRIPCIONLabel1.Text = ""
            If IsNumeric(GloContrato) = True Then
                If GloContrato > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.DAMETIPOSCLIENTESTableAdapter.Connection = CON
                    Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, Me.ContratoTextBox.Text)
                    CON.Dispose()
                    CON.Close()

                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If BndError = 1 Then
            If Me.LABEL19.BackColor = Color.Yellow Then
                Me.LABEL19.BackColor = Me.Panel5.BackColor
            Else
                Me.LABEL19.BackColor = Color.Yellow
            End If
        End If
    End Sub

    Private Sub Button6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            If DataGridView1.RowCount > 0 Then
                If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) Then
                    GloDes_Ser = DataGridView1.SelectedCells(6).Value
                    gloClv_Session = DataGridView1.SelectedCells(0).Value
                    gloClv_Detalle = DataGridView1.SelectedCells(29).Value
                    loctitulo = "Solo el Supervisor puede Bonificar"
                    locband_pant = 3
                    locBndBon1 = True
                    If IdSistema = "SA" And GloTipoUsuario = 1 Then
                        eAccesoAdmin = False
                    End If
                    FrmSupervisor.Show()
                Else
                    MsgBox("Seleccione el Concepto que deseas Bonificar ", MsgBoxStyle.Information)
                End If
            End If
            eBotonGuardar = False
        Catch ex As System.Exception
            Exit Sub
        End Try
    End Sub

    Private Sub Label13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label13.Click

    End Sub


    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, New System.Nullable(Of Long)(CType(Clv_FacturaToolStripTextBox.Text, Long)), DescripcionToolStripTextBox.Text)
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        BotonGrabar()
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect

    End Sub

    Private Sub TreeView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TreeView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub FolioTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub FolioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub Button8_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button8.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub ConfigureCrystalReportsOrdenes(ByVal op As String, ByVal Titulo As String, ByVal Clv_Orden1 As Long, ByVal Clv_TipSer1 As Integer, ByVal GloNom_TipSer As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0


            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            reportPath = RutaReportes + "\ReporteOrdenes.rpt"
            mySelectFormula = "Orden De Servicio: "


            Dim DS As New DataSet



            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CObj(0))
            BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, CShort(1))
            BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, CLng(Clv_Orden1))
            BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, CLng(Clv_Orden1))
            BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, CLng(OpOrdenar))

            Dim listatablas As New List(Of String)
            listatablas.Add("ReporteOrdSer")
            listatablas.Add("Comentarios_DetalleOrden")
            listatablas.Add("DameDatosGenerales_2")
            listatablas.Add("DetOrdSer")
            listatablas.Add("Trabajos")
            listatablas.Add("ClientesConElMismoPoste")
            DS = BaseII.ConsultaDS("ReporteOrdSer", listatablas)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(DS)



            mySelectFormula = "Orden De Servicio: "

            Dim CON12 As New SqlConnection(MiConexion)
            CON12.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON12
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.NewsoftvDataSet2.Dame_Impresora_Ordenes, Impresora, a)
            CON12.Dispose()
            CON12.Close()

            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio.", MsgBoxStyle.Information)
                Exit Sub
            Else
                'Dim conexion As New SqlConnection(MiConexion)
                'conexion.Open()
                'Dim consulta1 As New SqlCommand("select clv_orden from ordenes_clientesoftv where clv_orden =@clv_orden ", conexion)
                'Dim par3 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                'par3.Direction = ParameterDirection.Input
                'par3.Value = Clv_Orden1
                'consulta1.Parameters.Add(par3)
                'Dim contr1 As String = CStr(consulta1.ExecuteScalar())
                'Console.WriteLine("contrato es ", contr1)
                'If contr1 <= 0 Then
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 0, 0)
                'End If

            End If

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Mana_ImprimirOrdenes(ByVal Clv_Factura As Long)
        Try
            'Eric 10Dic2008 LAS ORDENES DE SERVICIO QUE SE GENERAN DESDE FACTURACION
            'NO SE IMPRIMIRAN DE FORMA AUTOM�TICA
            If IdSistema <> "VA" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Connection = CON
                Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Fill(Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura, New System.Nullable(Of Long)(CType(Clv_Factura, Long)))
                CON.Dispose()
                CON.Close()
                Dim FilaRow As DataRow
                'Me.TextBox1.Text = ""

                For Each FilaRow In Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura.Rows
                    If IsNumeric(FilaRow("Clv_Orden").ToString()) = True Then
                        ConfigureCrystalReportsOrdenes(0, "", FilaRow("Clv_Orden").ToString(), FilaRow("Clv_TipSer").ToString(), FilaRow("Concepto").ToString())
                    End If
                Next
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBajaTemporal.Click
        'FrmServiciosPPE.Show()
        Dim Respuesta As String = ""
        Respuesta = VALIDADESCONEXIONTemporal(GloContrato, gloClv_Session)
        If Respuesta = "SI" Then
            DAMETIPOSCLIENTEDAME()
            GloBnd = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'MsgBox(gloClv_Session & "," & gloClv_Servicio & "," & gloClv_llave & "," & gloClv_UnicaNet & "," & gloClave & "," & IdSistema & "," & GloAdelantados & "," & 0 & "," & Me.CLV_TIPOCLIENTELabel1.Text & "," & bndtodos & "," & Me.ContratoTextBox.Text & "," & BndError & "," & Msg)
            Me.PagosAdelantadosTableAdapter.Connection = CON
            Me.PagosAdelantadosTableAdapter.Fill(Me.NewsoftvDataSet.PagosAdelantados, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(0, Long)), New System.Nullable(Of Long)(CType(0, Long)), New System.Nullable(Of Long)(CType(0, Long)), New System.Nullable(Of Long)(CType(0, Long)), IdSistema, New System.Nullable(Of Integer)(CType(1, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, 1, Me.ContratoTextBox.Text, BndError, Msg)
            CON.Dispose()
            CON.Close()
            Dim CON10 As New SqlConnection(MiConexion)
            CON10.Open()
            Me.DameDetalleTableAdapter.Connection = CON10
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
            'GUARDE 
            Me.SumaDetalleTableAdapter.Connection = CON10
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)
            CON10.Dispose()
            CON10.Close()
            Me.Clv_Session.Text = 0
            Me.Clv_Session.Text = gloClv_Session
            If IsNumeric(BndError) = False Then BndError = 1
            If BndError = 1 Then
                Me.LABEL19.Text = Msg
                Me.Panel5.Visible = True
                Me.Bloque(False)
            ElseIf BndError = 2 Then
                MsgBox(Msg)
            Else
                Me.Bloque(True)
            End If
        End If
    End Sub

    Private Sub CMBPanel6_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles CMBPanel6.Paint

    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Me.CMBPanel6.Visible = False
        SiPagos = 0
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            SiPagos = 1
            Me.Cobra_PagosTableAdapter.Connection = CON
            Me.Cobra_PagosTableAdapter.Fill(Me.DataSetEdgar.Cobra_Pagos, New System.Nullable(Of Long)(CType(GloContrato, Long)), New System.Nullable(Of Long)(CType(Clv_Session.Text, Long)), New System.Nullable(Of Integer)(CType(Bnd, Integer)), New System.Nullable(Of Integer)(CType(CuantasTv, Integer)))
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
            CON.Dispose()
            CON.Close()
            Me.CMBPanel6.Visible = False
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim LOCCLAVE As Integer = 0
        If IsNumeric(DataGridView1.SelectedCells(4).Value) = False Then LOCCLAVE = 0 Else LOCCLAVE = DataGridView1.SelectedCells(4).Value
        If LOCCLAVE <> 2 Then
            MessageBox.Show("S�lo se puede Cobrar Adeudo a conceptos de Mensualidad.")
            Exit Sub
        End If
        FrmMotCan.Show()
    End Sub

    Private Sub CLV_DETALLETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_DETALLETextBox.TextChanged

    End Sub

    Private Sub Label14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label14.Click

    End Sub


    Private Sub LblFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LblFecha.Click

    End Sub

    Private Sub LblFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LblFecha.TextChanged
        If IsDate(LblFecha.Text) = True Then
            Me.Fecha_Venta.Value = LblFecha.Text
        End If
    End Sub

    Private Sub Fecha_Venta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_Venta.ValueChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                If bloqueado <> 1 Then
                    BUSCACLIENTES(0)
                    Me.Bloque(True)
                End If
            End If
        End If
    End Sub




    Private Sub ClvSessionDataGridViewTextBoxColumn1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClvSessionDataGridViewTextBoxColumn1.Disposed

    End Sub

    Private Sub ConRelClienteObs(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelClienteObs", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Obs", SqlDbType.VarChar, 500)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
            Me.TextBoxClienteObs.Text = parametro2.Value
            If Me.TextBoxClienteObs.Text.Length > 0 Then Me.TextBoxClienteObs.Text = "Observaci�n: " + Me.TextBoxClienteObs.Text
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub PreCobroAdeudo(ByVal Clv_Session As Long, ByVal Clv_Detalle As Integer, ByVal Clv_MotCan As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Clv_Detalle", SqlDbType.Int, CLV_DETALLE)
        BaseII.CreateMyParameter("@Clv_MotCan", SqlDbType.Int, Clv_MotCan)
        BaseII.Inserta("PreCobraAdeudo")

        'Dim conexion As New SqlConnection(MiConexion)
        'Dim comando As New SqlCommand("PreCobraAdeudo", conexion)
        'comando.CommandType = CommandType.StoredProcedure

        'Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        'parametro.Direction = ParameterDirection.Input
        'parametro.Value = Clv_Session
        'comando.Parameters.Add(parametro)

        'Dim parametro2 As New SqlParameter("@Clv_MotCan", SqlDbType.Int)
        'parametro2.Direction = ParameterDirection.Input
        'parametro2.Value = Clv_MotCan
        'comando.Parameters.Add(parametro2)

        'Try
        '    conexion.Open()
        '    comando.ExecuteNonQuery()
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        'Finally
        '    conexion.Close()
        '    conexion.Dispose()
        'End Try
    End Sub

    Private Function VALIDADESCONEXIONTemporal(ByVal oContrato As Long, ByVal oClv_Session As Long) As String
        VALIDADESCONEXIONTemporal = ""
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("VALIDADESCONEXIONTemporal", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oContrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@MSJ", SqlDbType.VarChar, 500)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@CLV_SESSION", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = oClv_Session
        comando.Parameters.Add(parametro3)
        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            VALIDADESCONEXIONTemporal = parametro2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            VALIDADESCONEXIONTemporal = ""
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Function

    Private Sub ChecaServiciosCancelados(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaServiciosCancelados", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Bnd", SqlDbType.Bit)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eBnd = parametro2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub CobraBajas(ByVal Clv_Session As Long, ByVal Contrato As Long, ByVal Clv_MotCan As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CobraBajas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_MotCan", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_MotCan
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub GrabaFacturasBajas(ByVal Contrato As Long, ByVal Clv_Session As Long, ByVal Cajera As String, ByVal Clv_Sucursal As Integer, ByVal Caja As Integer, ByVal Tipo As String, ByVal SerieV As String, ByVal FolioV As String, ByVal Clv_Vendedor As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("GrabaFacturasBajas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Session
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Cajera
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_Sucursal
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Caja", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Caja
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Tipo
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@SerieV", SqlDbType.VarChar, 5)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = SerieV
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@FolioV", SqlDbType.BigInt)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = FolioV
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
        parametro9.Direction = ParameterDirection.Input
        parametro9.Value = Clv_Vendedor
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Res", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
        parametro11.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro11)

        Dim parametro12 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro12.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro12)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            If CInt(parametro10.Value) = 1 Then
                MsgBox(parametro11.Value, MsgBoxStyle.Exclamation)
                Return
            End If
            GloClv_Factura = CLng(parametro12.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Public Sub CobroDeAdeudo()
        Try
            Dim CON As New SqlConnection(MiConexion)
            If IsNumeric(Me.Clv_Session.Text) = True Then ' And IsNumeric(Me.CLV_DETALLETextBox.Text) = True Then
                If Me.Clv_Session.Text > 0 Then 'And Me.CLV_DETALLETextBox.Text > 0 Then
                    ePideAparato = 0
                    Dim ERROR_1 As Integer = 0
                    Dim msgERROR_1 As String = Nothing

                    ChecaServiciosCancelados(GloContrato)

                    If eBnd = True Then
                        eBnd = False
                        CobraBajas(CLng(Me.Clv_Session.Text), GloContrato, eClvMotCan)
                        Me.Button2.Enabled = True
                        Me.Button6.Enabled = True
                    Else
                        PreCobroAdeudo(CLng(Me.Clv_Session.Text), CLV_DETALLETextBox.Text, eClvMotCan)
                    End If


                    CON.Open()
                    'Me.CobraAdeudoTableAdapter.Connection = CON
                    'Me.CobraAdeudoTableAdapter.Fill(Me.DataSetEdgar.CobraAdeudo, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(Me.CLV_DETALLETextBox.Text, Long)), ERROR_1, msgERROR_1, ePideAparato, eClv_Detalle)
                    Me.DameDetalleTableAdapter.Connection = CON
                    Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
                    Me.SumaDetalleTableAdapter.Connection = CON
                    Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
                    CON.Dispose()
                    CON.Close()

                    If ERROR_1 = 1 Then
                        Me.LABEL19.Text = msgERROR_1
                        Me.Panel5.Visible = True
                        Me.Bloque(False)
                    ElseIf ERROR_1 = 2 Then
                        MsgBox(msgERROR_1)
                    End If
                    'VALIDACION PARA VALLARTA. PREGUNTA SI EL CLIENTE LLEVA CONSIGO EL APARATO, SI NO, SE GENERA UNA ORDEN DE RETIRO DE APARATO O CABLEMODEM
                    If ePideAparato = 1 Then
                        eRes = MsgBox("�El Cliente trae con sigo el Aparato?", MsgBoxStyle.YesNo, "Atenci�n")
                        If eRes = 6 Then
                            Dim CON2 As New SqlConnection(MiConexion)
                            CON2.Open()
                            Me.EntregaAparatoTableAdapter.Connection = CON2
                            Me.EntregaAparatoTableAdapter.Fill(Me.EricDataSet2.EntregaAparato, CLng(Me.Clv_Session.Text), eClv_Detalle)
                            CON2.Close()
                        End If
                    End If

                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ContratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(ContratoTextBox, Asc(LCase(e.KeyChar)), "N")))
       
    End Sub

    Private Sub ContratoTextBox_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContratoTextBox.Resize

    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

    End Sub

    Private Sub COLONIALabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles COLONIALabel1.Click

    End Sub
    Private Sub ChecaOrdenRetiro(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaOrdenRetiro", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 250)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = CInt(parametro2.Value)
            eMsj = parametro3.Value.ToString
            If eRes = 1 Then FrmMsj.Show()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub ChecaAdelantarPagosDif(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaAdelantarPagosDif", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim paramrtro2 As New SqlParameter("@Res", SqlDbType.Int)
        paramrtro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(paramrtro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = String.Empty
            eRes = CInt(paramrtro2.Value)
            eMsj = parametro3.Value.ToString
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ChecaPromocion(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaPromocion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = ""
            eRes = parametro2.Value
            eMsj = parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub


    Private Sub AplazaElPagoPromocion(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("AplazaElPagoPromocion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub InsertaPreRelClientePromocion(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPreRelClientePromocion", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub LenaFolios()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC Folio_Disponible " & CStr(Me.ComboBox1.SelectedValue) & ", '" & CStr(Me.ComboBox2.SelectedValue) & "'")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBox3.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        'Me.ComboBox3.Text = ""
    End Sub

    Private Function uspChecaFolioUsado(ByVal prmClvVendedor As Integer, ByVal prmSerie As String, ByVal prmFolio As Integer) As Integer
        ' INT, @ VARCHAR(10),@ INT, @ INT
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("uspChecaFolioUsado", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim prm1 As New SqlParameter("@clvVendedor", SqlDbType.Int)
        Dim prm2 As New SqlParameter("@serie", SqlDbType.VarChar, 10)
        Dim prm3 As New SqlParameter("@folio", SqlDbType.Int)
        Dim prm4 As New SqlParameter("@bndFolios", SqlDbType.Int)

        prm1.Direction = ParameterDirection.Input
        prm2.Direction = ParameterDirection.Input
        prm3.Direction = ParameterDirection.Input
        prm4.Direction = ParameterDirection.Output

        prm1.Value = prmClvVendedor
        prm2.Value = prmSerie
        prm3.Value = prmFolio

        CMD.Parameters.Add(prm1)
        CMD.Parameters.Add(prm2)
        CMD.Parameters.Add(prm3)
        CMD.Parameters.Add(prm4)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            uspChecaFolioUsado = CInt(prm4.Value.ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Function

    Private Function BUSFACFISCAL(ByVal CLV_FACTURA As Integer) As Integer
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, CLV_FACTURA)
        BaseII.CreateMyParameter("@IDENT", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("BUSFACFISCAL")
        Return CInt(BaseII.dicoPar("@IDENT").ToString())
    End Function

    Private Sub REPORTEFacturaFiscal(ByVal CLV_FACTURA As Integer)

        If impresorafiscal.Length = 0 Then
            MessageBox.Show("No se ha asignado la impresora de facturas fiscales.")
            Exit Sub
        End If

        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        Dim rDocument As New ReportDocument

        tableNameList.Add("DatosFiscales")
        tableNameList.Add("Factura")
        tableNameList.Add("Importes")
        tableNameList.Add("Datos")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, CLV_FACTURA)
        dSet = BaseII.ConsultaDS("REPORTEFacturaFiscal", tableNameList)

        rDocument.Load(RutaReportes + "\REPORTEFacturaFiscal.rpt")
        rDocument.SetDataSource(dSet)
        rDocument.PrintOptions.PrinterName = impresorafiscal
        rDocument.PrintToPrinter(1, False, 0, 0)
    End Sub

    Private Sub CHECADevolucionAparatosCliente(ByVal CLV_SESSION As Integer, ByVal CONTRATO As Integer, ByVal ACTIVO As Boolean)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        BaseII.CreateMyParameter("@ACTIVO", SqlDbType.Bit, ACTIVO)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("CHECADevolucionAparatosCliente")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
    End Sub

    Private Sub BtnPagMaterial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPagMaterial.Click
        gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
        Dim Frmcobro As New FrmDetCobroDescMate
        Frmcobro.ShowDialog()
        Exit Sub
    End Sub

    'Public Sub RecalculaTotales_FacturacionNormal()
    '    'Barremos el Grid de SumaDetalle para mostrar los totales a pagar

    '    Dim i As Integer
    '    Try
    '        i = 0
    '        If Me.SumaDetalleDataGridView.Rows.Count > 0 Then

    '            'Asignamos SumaDetalle al Nuevo grid base
    '            Me.DataGridView2.DataSource = Me.SumaDetalleDataGridView.DataSource

    '            'Recorremos los totales generados en Facturaci�n Normal
    '            While i < Me.DataGridView2.RowCount


    '                If Me.DataGridView2.Rows(i).Cells("Descripcion").Value.ToString = "Total de Puntos Aplicados " Then
    '                    Me.ImportePuntosAplicados.Text = "$" & Me.DataGridView2.Rows(i).Cells("Total").Value.ToString

    '                ElseIf Me.DataGridView2.Rows(i).Cells("Descripcion").Value.ToString = "Total Bonificado " Then
    '                    Me.ImporteTotalBonificado.Text = "$" & Me.DataGridView2.Rows(i).Cells("Total").Value.ToString

    '                ElseIf Me.DataGridView2.Rows(i).Cells("Descripcion").Value.ToString = "Importe Total" Then
    '                    Me.LabelGRan_Total.Text = "$" & Me.DataGridView2.Rows(i).Cells("Total").Value.ToString

    '                ElseIf Me.DataGridView2.Rows(i).Cells("Descripcion").Value.ToString = "Subtotal " Then
    '                    Me.LabelTotal.Text = "$" & Me.DataGridView2.Rows(i).Cells("Total").Value.ToString

    '                End If

    '                i = i + 1
    '            End While

    '        End If
    '    Catch ex As Exception
    '        'MsgBox(ex.Message)
    '    End Try
    'End Sub

    Private Sub usp_ImpresoraTermica()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Nom_caja", SqlDbType.VarChar, Me.LblNomCaja.Text)
        BaseII.CreateMyParameter("@ImpTermi", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Usp_ImpresoraTermicaNom")
        ImprTermi = CInt(BaseII.dicoPar("@ImpTermi").ToString)

    End Sub

    Private Sub Button13_Click(sender As System.Object, e As System.EventArgs) Handles Button13.Click
        Dim FRM2 As New FormInformacion
        FRM2.ShowDialog()
    End Sub

    Public Function Valida_Zonas() As Boolean
        Try

        

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, GloContrato)
        BaseII.CreateMyParameter("@Zonas", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("Valida_Zonas")
            Return BaseII.dicoPar("@Zonas").ToString
        Catch ex As Exception

        End Try


    End Function
    Public Sub ChecaAdeudoDeMaterial(ByVal Contrato As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        conexion.Open()
        Dim con As New SqlCommand("SELECT CONTRATO FROM tblDescargaMaterial  WHERE ( contrato = @CONTRATO  AND  CONTRATO NOT IN   (SELECT contrato   FROM tblPagosDiferidosCobroMaterial WHERE contrato = @CONTRATO )) AND pagado = 0 and totalCobrar >0  AND esCable =1 OR CONTRATO  IN (SELECT CONTRATO  FROM tblDescargaMaterialExtensiones WHERE CONTRATO = @CONTRATO and pagado = 0) ", conexion)
        Dim par2 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Contrato
        con.Parameters.Add(par2)
        Dim contr As String = CStr(con.ExecuteScalar())
        Console.WriteLine("contrato es ", contr)

        If contr = 0 Then
            Dim conexion1 As New SqlConnection(MiConexion)
            conexion1.Open()
            Dim con1 As New SqlCommand("select contrato from Tbl_DescargaDeMaterialAntigua where contrato  = @CONTRATO ", conexion1)
            Dim par21 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
            par21.Direction = ParameterDirection.Input
            par21.Value = Contrato
            con1.Parameters.Add(par21)
            Dim contr1 As String = CStr(con1.ExecuteScalar())
            Console.WriteLine("contrato es ", contr1)
            contr = contr1
        End If

        'If contr = GloContrato Then

        'MsgBox("�Decea pagar adeudo de materian en parcialidades?", vbYesNo)
        'If MsgBox("�Desea pagar adeudo de material en parcialidades?", vbYesNo) = vbYes Then

        '    FrmAdeudoDeCable.ShowDialog()
        '    If Parcialidades = 1 Then
        '        ActualizarDetalle(gloClv_Session, "CCPM", SaldoAbonadoCableV + SaldoAbonadoCableR + SaldoAbonadoCableAnterior)
        '        Dim CON10 As New SqlConnection(MiConexion)
        '        CON10.Open()
        '        Me.DameDetalleTableAdapter.Connection = CON10
        '        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
        '        'GUARDE 
        '        Me.SumaDetalleTableAdapter.Connection = CON10
        '        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)
        '        CON10.Dispose()
        '        CON10.Close()
        '    End If
        'End If
        'End If

    End Sub
    Public Sub ActualizarDetalle(ByVal SESSION As Integer, ByVal TRABAJO As String, ByVal IMPORTE As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, SESSION)
        BaseII.CreateMyParameter("@clv_txt", SqlDbType.VarChar, TRABAJO)
        BaseII.CreateMyParameter("@importe", SqlDbType.Money, IMPORTE)
        BaseII.ProcedimientoOutPut("AgregarCobroParcialDeMaterial")
    End Sub

    Private Sub SOLOINTERNETLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub consultaDatosGeneralesSucursal(ByVal prmClvSucursal As Integer, ByVal prmClvFactura As Integer)

        Dim dtDatosGenerales As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clvSucursal", SqlDbType.Int, prmClvSucursal)
        BaseII.CreateMyParameter("@clvFactura", SqlDbType.Int, prmClvFactura)
        dtDatosGenerales = BaseII.ConsultaDT("uspConsultaTblRelSucursalDatosGenerales")


        If dtDatosGenerales.Rows.Count > 0 Then
            Me.RCalleSucur = dtDatosGenerales.Rows(0)("calle").ToString
            Me.RNumSucur = dtDatosGenerales.Rows(0)("numero").ToString
            Me.RColSucur = dtDatosGenerales.Rows(0)("colonia").ToString
            Me.RCPSucur = CInt(dtDatosGenerales.Rows(0)("cp").ToString)
            Me.RMuniSucur = dtDatosGenerales.Rows(0)("municipio").ToString
            Me.RCiudadSucur = dtDatosGenerales.Rows(0)("ciudad").ToString
            Me.RTelSucur = dtDatosGenerales.Rows(0)("telefono").ToString
        End If
    End Sub
End Class
