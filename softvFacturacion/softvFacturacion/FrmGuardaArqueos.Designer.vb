﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGuardaArqueos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxTotales = New System.Windows.Forms.GroupBox()
        Me.txtTotalCobrado = New System.Windows.Forms.TextBox()
        Me.lblTotalCobrado = New System.Windows.Forms.Label()
        Me.txtAutoriza = New System.Windows.Forms.TextBox()
        Me.lblAutoriza = New System.Windows.Forms.Label()
        Me.txtTotalGastos = New System.Windows.Forms.TextBox()
        Me.lblTotalGastos = New System.Windows.Forms.Label()
        Me.txtEntregasParciales = New System.Windows.Forms.TextBox()
        Me.lblEntregasParciales = New System.Windows.Forms.Label()
        Me.txtTotalCheques = New System.Windows.Forms.TextBox()
        Me.lblTotalCheques = New System.Windows.Forms.Label()
        Me.txtTotalTarjeta = New System.Windows.Forms.TextBox()
        Me.lblTotalTarjeta = New System.Windows.Forms.Label()
        Me.txtTotalEfectivo = New System.Windows.Forms.TextBox()
        Me.lblTotalEfectivo = New System.Windows.Forms.Label()
        Me.dtpFechaGeneracion = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaGeneracion = New System.Windows.Forms.Label()
        Me.dtpFechaCorte = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaCorte = New System.Windows.Forms.Label()
        Me.txtCajera = New System.Windows.Forms.TextBox()
        Me.lblCajera = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxTotales.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxTotales
        '
        Me.gbxTotales.Controls.Add(Me.txtTotalCobrado)
        Me.gbxTotales.Controls.Add(Me.lblTotalCobrado)
        Me.gbxTotales.Controls.Add(Me.txtAutoriza)
        Me.gbxTotales.Controls.Add(Me.lblAutoriza)
        Me.gbxTotales.Controls.Add(Me.txtTotalGastos)
        Me.gbxTotales.Controls.Add(Me.lblTotalGastos)
        Me.gbxTotales.Controls.Add(Me.txtEntregasParciales)
        Me.gbxTotales.Controls.Add(Me.lblEntregasParciales)
        Me.gbxTotales.Controls.Add(Me.txtTotalCheques)
        Me.gbxTotales.Controls.Add(Me.lblTotalCheques)
        Me.gbxTotales.Controls.Add(Me.txtTotalTarjeta)
        Me.gbxTotales.Controls.Add(Me.lblTotalTarjeta)
        Me.gbxTotales.Controls.Add(Me.txtTotalEfectivo)
        Me.gbxTotales.Controls.Add(Me.lblTotalEfectivo)
        Me.gbxTotales.Controls.Add(Me.dtpFechaGeneracion)
        Me.gbxTotales.Controls.Add(Me.lblFechaGeneracion)
        Me.gbxTotales.Controls.Add(Me.dtpFechaCorte)
        Me.gbxTotales.Controls.Add(Me.lblFechaCorte)
        Me.gbxTotales.Controls.Add(Me.txtCajera)
        Me.gbxTotales.Controls.Add(Me.lblCajera)
        Me.gbxTotales.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxTotales.Location = New System.Drawing.Point(12, 12)
        Me.gbxTotales.Name = "gbxTotales"
        Me.gbxTotales.Size = New System.Drawing.Size(314, 314)
        Me.gbxTotales.TabIndex = 0
        Me.gbxTotales.TabStop = False
        Me.gbxTotales.Text = "Totales"
        '
        'txtTotalCobrado
        '
        Me.txtTotalCobrado.Location = New System.Drawing.Point(171, 279)
        Me.txtTotalCobrado.Name = "txtTotalCobrado"
        Me.txtTotalCobrado.ReadOnly = True
        Me.txtTotalCobrado.Size = New System.Drawing.Size(129, 22)
        Me.txtTotalCobrado.TabIndex = 9
        Me.txtTotalCobrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTotalCobrado
        '
        Me.lblTotalCobrado.AutoSize = True
        Me.lblTotalCobrado.Location = New System.Drawing.Point(45, 281)
        Me.lblTotalCobrado.Name = "lblTotalCobrado"
        Me.lblTotalCobrado.Size = New System.Drawing.Size(116, 16)
        Me.lblTotalCobrado.TabIndex = 18
        Me.lblTotalCobrado.Text = "Total Cobrado :"
        '
        'txtAutoriza
        '
        Me.txtAutoriza.Location = New System.Drawing.Point(171, 251)
        Me.txtAutoriza.Name = "txtAutoriza"
        Me.txtAutoriza.ReadOnly = True
        Me.txtAutoriza.Size = New System.Drawing.Size(129, 22)
        Me.txtAutoriza.TabIndex = 8
        Me.txtAutoriza.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblAutoriza
        '
        Me.lblAutoriza.AutoSize = True
        Me.lblAutoriza.Location = New System.Drawing.Point(89, 253)
        Me.lblAutoriza.Name = "lblAutoriza"
        Me.lblAutoriza.Size = New System.Drawing.Size(72, 16)
        Me.lblAutoriza.TabIndex = 16
        Me.lblAutoriza.Text = "Autoriza :"
        '
        'txtTotalGastos
        '
        Me.txtTotalGastos.Location = New System.Drawing.Point(171, 223)
        Me.txtTotalGastos.Name = "txtTotalGastos"
        Me.txtTotalGastos.ReadOnly = True
        Me.txtTotalGastos.Size = New System.Drawing.Size(129, 22)
        Me.txtTotalGastos.TabIndex = 7
        Me.txtTotalGastos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTotalGastos
        '
        Me.lblTotalGastos.AutoSize = True
        Me.lblTotalGastos.Location = New System.Drawing.Point(56, 225)
        Me.lblTotalGastos.Name = "lblTotalGastos"
        Me.lblTotalGastos.Size = New System.Drawing.Size(105, 16)
        Me.lblTotalGastos.TabIndex = 14
        Me.lblTotalGastos.Text = "Total Gastos :"
        '
        'txtEntregasParciales
        '
        Me.txtEntregasParciales.Location = New System.Drawing.Point(171, 195)
        Me.txtEntregasParciales.Name = "txtEntregasParciales"
        Me.txtEntregasParciales.ReadOnly = True
        Me.txtEntregasParciales.Size = New System.Drawing.Size(129, 22)
        Me.txtEntregasParciales.TabIndex = 6
        Me.txtEntregasParciales.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblEntregasParciales
        '
        Me.lblEntregasParciales.AutoSize = True
        Me.lblEntregasParciales.Location = New System.Drawing.Point(13, 197)
        Me.lblEntregasParciales.Name = "lblEntregasParciales"
        Me.lblEntregasParciales.Size = New System.Drawing.Size(148, 16)
        Me.lblEntregasParciales.TabIndex = 12
        Me.lblEntregasParciales.Text = "Entregas Parciales :"
        '
        'txtTotalCheques
        '
        Me.txtTotalCheques.Location = New System.Drawing.Point(171, 167)
        Me.txtTotalCheques.Name = "txtTotalCheques"
        Me.txtTotalCheques.ReadOnly = True
        Me.txtTotalCheques.Size = New System.Drawing.Size(129, 22)
        Me.txtTotalCheques.TabIndex = 5
        Me.txtTotalCheques.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTotalCheques
        '
        Me.lblTotalCheques.AutoSize = True
        Me.lblTotalCheques.Location = New System.Drawing.Point(44, 169)
        Me.lblTotalCheques.Name = "lblTotalCheques"
        Me.lblTotalCheques.Size = New System.Drawing.Size(117, 16)
        Me.lblTotalCheques.TabIndex = 10
        Me.lblTotalCheques.Text = "Total Cheques :"
        '
        'txtTotalTarjeta
        '
        Me.txtTotalTarjeta.Location = New System.Drawing.Point(171, 139)
        Me.txtTotalTarjeta.Name = "txtTotalTarjeta"
        Me.txtTotalTarjeta.ReadOnly = True
        Me.txtTotalTarjeta.Size = New System.Drawing.Size(129, 22)
        Me.txtTotalTarjeta.TabIndex = 4
        Me.txtTotalTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTotalTarjeta
        '
        Me.lblTotalTarjeta.AutoSize = True
        Me.lblTotalTarjeta.Location = New System.Drawing.Point(55, 141)
        Me.lblTotalTarjeta.Name = "lblTotalTarjeta"
        Me.lblTotalTarjeta.Size = New System.Drawing.Size(106, 16)
        Me.lblTotalTarjeta.TabIndex = 8
        Me.lblTotalTarjeta.Text = "Total Tarjeta :"
        '
        'txtTotalEfectivo
        '
        Me.txtTotalEfectivo.Location = New System.Drawing.Point(171, 111)
        Me.txtTotalEfectivo.Name = "txtTotalEfectivo"
        Me.txtTotalEfectivo.ReadOnly = True
        Me.txtTotalEfectivo.Size = New System.Drawing.Size(129, 22)
        Me.txtTotalEfectivo.TabIndex = 3
        Me.txtTotalEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTotalEfectivo
        '
        Me.lblTotalEfectivo.AutoSize = True
        Me.lblTotalEfectivo.Location = New System.Drawing.Point(49, 113)
        Me.lblTotalEfectivo.Name = "lblTotalEfectivo"
        Me.lblTotalEfectivo.Size = New System.Drawing.Size(112, 16)
        Me.lblTotalEfectivo.TabIndex = 6
        Me.lblTotalEfectivo.Text = "Total Efectivo :"
        '
        'dtpFechaGeneracion
        '
        Me.dtpFechaGeneracion.Enabled = False
        Me.dtpFechaGeneracion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaGeneracion.Location = New System.Drawing.Point(171, 83)
        Me.dtpFechaGeneracion.Name = "dtpFechaGeneracion"
        Me.dtpFechaGeneracion.Size = New System.Drawing.Size(129, 22)
        Me.dtpFechaGeneracion.TabIndex = 2
        '
        'lblFechaGeneracion
        '
        Me.lblFechaGeneracion.AutoSize = True
        Me.lblFechaGeneracion.Location = New System.Drawing.Point(18, 86)
        Me.lblFechaGeneracion.Name = "lblFechaGeneracion"
        Me.lblFechaGeneracion.Size = New System.Drawing.Size(143, 16)
        Me.lblFechaGeneracion.TabIndex = 4
        Me.lblFechaGeneracion.Text = "Fecha Generación :"
        '
        'dtpFechaCorte
        '
        Me.dtpFechaCorte.Enabled = False
        Me.dtpFechaCorte.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaCorte.Location = New System.Drawing.Point(171, 55)
        Me.dtpFechaCorte.Name = "dtpFechaCorte"
        Me.dtpFechaCorte.Size = New System.Drawing.Size(129, 22)
        Me.dtpFechaCorte.TabIndex = 1
        '
        'lblFechaCorte
        '
        Me.lblFechaCorte.AutoSize = True
        Me.lblFechaCorte.Location = New System.Drawing.Point(61, 57)
        Me.lblFechaCorte.Name = "lblFechaCorte"
        Me.lblFechaCorte.Size = New System.Drawing.Size(100, 16)
        Me.lblFechaCorte.TabIndex = 2
        Me.lblFechaCorte.Text = "Fecha Corte :"
        '
        'txtCajera
        '
        Me.txtCajera.Location = New System.Drawing.Point(171, 27)
        Me.txtCajera.Name = "txtCajera"
        Me.txtCajera.ReadOnly = True
        Me.txtCajera.Size = New System.Drawing.Size(129, 22)
        Me.txtCajera.TabIndex = 0
        Me.txtCajera.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblCajera
        '
        Me.lblCajera.AutoSize = True
        Me.lblCajera.Location = New System.Drawing.Point(76, 29)
        Me.lblCajera.Name = "lblCajera"
        Me.lblCajera.Size = New System.Drawing.Size(85, 16)
        Me.lblCajera.TabIndex = 0
        Me.lblCajera.Text = "Cajera (o) :"
        '
        'btnAceptar
        '
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(45, 332)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(100, 33)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(184, 332)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(100, 33)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'FrmGuardaArqueos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(337, 371)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbxTotales)
        Me.Name = "FrmGuardaArqueos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Corte Final del Día"
        Me.gbxTotales.ResumeLayout(False)
        Me.gbxTotales.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxTotales As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotalCobrado As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalCobrado As System.Windows.Forms.Label
    Friend WithEvents txtAutoriza As System.Windows.Forms.TextBox
    Friend WithEvents lblAutoriza As System.Windows.Forms.Label
    Friend WithEvents txtTotalGastos As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalGastos As System.Windows.Forms.Label
    Friend WithEvents txtEntregasParciales As System.Windows.Forms.TextBox
    Friend WithEvents lblEntregasParciales As System.Windows.Forms.Label
    Friend WithEvents txtTotalCheques As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalCheques As System.Windows.Forms.Label
    Friend WithEvents txtTotalTarjeta As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalTarjeta As System.Windows.Forms.Label
    Friend WithEvents txtTotalEfectivo As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalEfectivo As System.Windows.Forms.Label
    Friend WithEvents dtpFechaGeneracion As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFechaGeneracion As System.Windows.Forms.Label
    Friend WithEvents dtpFechaCorte As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFechaCorte As System.Windows.Forms.Label
    Friend WithEvents txtCajera As System.Windows.Forms.TextBox
    Friend WithEvents lblCajera As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class
