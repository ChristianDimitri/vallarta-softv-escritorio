Imports System.Data.SqlClient

Public Class FrmSupervisor
    Dim bnd As Integer = 0
    Private Sub Acceso()
        Try
            bnd = 1
            If eAccesoAdmin = True And locband_pant <> 5 Then
                bnd = 1
            ElseIf GloTipoUsuario = 40 Then
                bnd = 1
            ElseIf IdSistema = "SA" And locband_pant = 3 Then
                GloClv_Txt = Me.Clv_UsuarioTextBox.Text
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.VerAcceso2TableAdapter.Connection = CON
                Me.VerAcceso2TableAdapter.Fill(Me.ProcedimientosArnoldo3.VerAcceso2, GloClv_Txt, Me.PasaporteTextBox.Text, bnd, locnomsupervisor)
                CON.Close()
                LocSupBon = GloClv_Txt
            ElseIf locband_pant = 5 Then
                veracceso2()
            Else
                GloClv_Txt = Me.Clv_UsuarioTextBox.Text
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.VerAcceso1TableAdapter.Connection = CON
                Me.VerAcceso1TableAdapter.Fill(Me.NewsoftvDataSet.VerAcceso1, GloClv_Txt, Me.PasaporteTextBox.Text, bnd, locnomsupervisor)
                LocSupBon = GloClv_Txt
                CON.Close()
                'veracceso2()
            End If
            bnd = 1
                If bnd = 1 Then
                    If locband_pant = 1 Then
                        My.Forms.BwrEntregasParciales.Show()
                        Me.Close()
                    ElseIf locband_pant = 2 Then
                        My.Forms.FrmArqueo.Show()
                        Me.Close()
                    ElseIf locband_pant = 3 Then
                        FrmBonificacion.Show()
                        Me.Close()
                ElseIf locband_pant = 4 Then
                    If GloTipoUsuario = 45 Then
                        FrmCortesdeFacturasporSucursal.Show()
                    Else
                        FrmCortesdeFacturas.Show()
                    End If

                    Me.Close()
                ElseIf locband_pant = 5 Then
                    If GloTipoUsuario = 45 Then
                        BrwFacturas_Cancelar_Sucursal.Show()
                    Else
                        BrwFacturas_Cancelar.Show()
                    End If

                    Me.Close() 'cancelar,
                    ElseIf locband_pant = 6 Then
                        'reimprimir 
                    'BrwFacturas_Cancelar.Show()
                    If GloTipoUsuario = 45 Then
                        BrwFacturas_Cancelar_Sucursal.Show()
                    Else
                        BrwFacturas_Cancelar.Show()
                    End If
                        Me.Close()
                    ElseIf locband_pant = 7 Then

                    ElseIf locband_pant = 8 Then
                        BndSupervisorFac = True
                        GloClvSupAuto = GloClv_Txt
                        Me.Close()
                    ElseIf locband_pant = 10 Then
                        BrwFacturasCancelarRE.Show()
                        Me.Close()
                    ElseIf locband_pant = 11 Then
                        LocbndPolizaCiudad = False
                        LocBndrelingporconceptos = True
                        FrmSelCiudad.Show()
                        Me.Close()
                    ElseIf locband_pant = 12 Then
                        LocBanderaRep1 = 2
                        FrmSelFechas.Show()
                        Me.Close()
                    ElseIf locband_pant = 13 Then
                        LocBanderaRep1 = 0
                        FrmSelFechas.Show()
                        Me.Close()
                    ElseIf locband_pant = 14 Then
                        LocBanderaRep1 = 1
                        FrmSelFechas.Show()
                        Me.Close()
                    ElseIf locband_pant = 15 Then
                        FrmFechasCobroMaterial.Show()
                        Me.Close()
                    End If
                Else
                    MsgBox(" Acceso Denegado ", MsgBoxStyle.Information)
                    'End
                End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub veracceso2()
        GloClv_Txt = Me.Clv_UsuarioTextBox.Text
        Dim con1 As New SqlConnection(MiConexion)
        Dim Cmd As New SqlClient.SqlCommand()
        Try
            con1.Open()
            Cmd = New SqlClient.SqlCommand()
            With Cmd
                .CommandText = "VerAcceso5"
                .Connection = con1
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClv_Txt
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("@Pas", SqlDbType.VarChar)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = Me.PasaporteTextBox.Text
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@bnd", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@nomsupervisor", SqlDbType.VarChar)
                prm4.Direction = ParameterDirection.Output
                prm4.Value = ""
                .Parameters.Add(prm4)

                Dim i As Integer = Cmd.ExecuteNonQuery()

                LocSupBon = GloClv_Txt
                bnd = CInt(prm3.Value.ToString)
                locnomsupervisor = prm4.Value.ToString
            End With
            con1.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Clv_UsuarioLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FrmSupervisor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eAccesoAdmin = True
        If eAccesoAdmin = True Then
            Acceso()
        Else
            If locband_pant = 8 Then
                MsgBox("El Cliente Actual Es De Cargo Recurrente, Se Requiere Una Autorización Del Supervisor Para Ser Cobrado", MsgBoxStyle.Information)
            End If
        End If
        colorea(Me)
        Me.Text = loctitulo
    End Sub

    Private Sub Clv_UsuarioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_UsuarioTextBox.TextChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Acceso()
    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasaporteTextBox.KeyPress
        If Asc(e.KeyChar.ToString) = 13 Then
            Acceso()
        End If
    End Sub



    Private Sub PasaporteTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasaporteTextBox.TextChanged

    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Acceso()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()

    End Sub

    
End Class