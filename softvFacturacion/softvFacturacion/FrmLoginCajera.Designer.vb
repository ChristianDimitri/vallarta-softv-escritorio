<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLoginCajera
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim CMBClv_UsuarioLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLoginCajera))
        Me.CMBPasswordLabel = New System.Windows.Forms.Label
        Me.Cancel = New System.Windows.Forms.Button
        Me.OK = New System.Windows.Forms.Button
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox
        Me.PasaporteTextBox = New System.Windows.Forms.TextBox
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox
        CMBClv_UsuarioLabel = New System.Windows.Forms.Label
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBClv_UsuarioLabel
        '
        CMBClv_UsuarioLabel.AutoSize = True
        CMBClv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_UsuarioLabel.Location = New System.Drawing.Point(259, 37)
        CMBClv_UsuarioLabel.Name = "CMBClv_UsuarioLabel"
        CMBClv_UsuarioLabel.Size = New System.Drawing.Size(111, 15)
        CMBClv_UsuarioLabel.TabIndex = 10
        CMBClv_UsuarioLabel.Text = "Login Cajera(o):"
        '
        'CMBPasswordLabel
        '
        Me.CMBPasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBPasswordLabel.Location = New System.Drawing.Point(259, 91)
        Me.CMBPasswordLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBPasswordLabel.Name = "CMBPasswordLabel"
        Me.CMBPasswordLabel.Size = New System.Drawing.Size(168, 27)
        Me.CMBPasswordLabel.TabIndex = 16
        Me.CMBPasswordLabel.Text = "&Contraseña Cajera(o):"
        Me.CMBPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.Orange
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.Location = New System.Drawing.Point(404, 176)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(152, 35)
        Me.Cancel.TabIndex = 3
        Me.Cancel.Text = "&CANCELAR"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.Orange
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(241, 176)
        Me.OK.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(155, 35)
        Me.OK.TabIndex = 2
        Me.OK.Text = "&ACEPTAR"
        Me.OK.UseVisualStyleBackColor = False
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(13, 12)
        Me.LogoPictureBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(220, 221)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LogoPictureBox.TabIndex = 13
        Me.LogoPictureBox.TabStop = False
        '
        'PasaporteTextBox
        '
        Me.PasaporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PasaporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasaporteTextBox.Location = New System.Drawing.Point(262, 121)
        Me.PasaporteTextBox.MaxLength = 10
        Me.PasaporteTextBox.Name = "PasaporteTextBox"
        Me.PasaporteTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasaporteTextBox.Size = New System.Drawing.Size(97, 22)
        Me.PasaporteTextBox.TabIndex = 1
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_UsuarioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(262, 55)
        Me.Clv_UsuarioTextBox.MaxLength = 15
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(116, 22)
        Me.Clv_UsuarioTextBox.TabIndex = 0
        '
        'FrmLoginCajera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 247)
        Me.Controls.Add(Me.CMBPasswordLabel)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.Controls.Add(Me.PasaporteTextBox)
        Me.Controls.Add(CMBClv_UsuarioLabel)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Name = "FrmLoginCajera"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso Cajera(o)"
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents PasaporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
End Class
