﻿Public Class FrmFiltroEntregasGlobales

    Private Sub FrmFiltroEntregasGlobales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        llenaComboCajeras()
    End Sub

    Private Sub llenaComboCajeras()
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (INICIO)
        ControlEfectivoClass.limpiaParametros()
        Me.cmbCajera.DisplayMember = "nombre"
        Me.cmbCajera.ValueMember = "clvUsuario"
        Me.cmbCajera.DataSource = ControlEfectivoClass.ConsultaDT("uspLlenaComboCajera")
        '''''LLENAMOS EL COMBO DE LAS CAJERAS (FIN)
    End Sub

    Private Sub uspReporteTiposGastos(ByVal prmClvUsuario As String, ByVal prmFechaIni As Date, ByVal prmFechaFin As Date)
        '''''MANDAMOS EL REPORTE (INICIO)
        ControlEfectivoClass.limpiaParametros() ''LIMPIAMOS LA LISTA DE PARÁMETROS

        ControlEfectivoClass.CreateMyParameter("@clvUsuario", SqlDbType.VarChar, prmClvUsuario, 5) ''MANDAMOS LOS PARÁMETROS
        ControlEfectivoClass.CreateMyParameter("@fechaIni", SqlDbType.DateTime, prmFechaIni)
        ControlEfectivoClass.CreateMyParameter("@fechaFIn", SqlDbType.DateTime, prmFechaFin)

        Dim listaTablas As New List(Of String) ''MANDAMOS EL NOMBRE DE LAS TABLAS QUE DEVOLVERÁ EL DATASET DEL REPORTE
        listaTablas.Add("uspReporteEntregasGlobales")

        Dim DataSet As New DataSet ''LLENAMOS EL DATASET QUE LLENARÁ EL REPORTE
        DataSet = ControlEfectivoClass.ConsultaDS("uspReporteEntregasGlobales", listaTablas)

        Dim diccioFormulasReporte As New Dictionary(Of String, String) ''LENAMOS EL DICCIONARIO QUE CONTENDRÁ LAS FÓRMULAS QUE REQUIERE EL REPORTE
        diccioFormulasReporte.Add("Ciudad", LocNomEmpresa)
        diccioFormulasReporte.Add("Empresa", GloNomSucursal)
        diccioFormulasReporte.Add("fechaFin", prmFechaFin)
        diccioFormulasReporte.Add("fechaIni", prmFechaIni)

        ControlEfectivoClass.llamarReporteCentralizado(RutaReportes + "/rptReporteEntregasGlobales", DataSet, diccioFormulasReporte) ''MANDAMOS LLAMAR EL REPORTE
        '''''MANDAMOS EL REPORTE (INICIO)
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If CDate(Me.dtpFechaInicial.Value.ToShortDateString) > CDate(Me.dtpFechaFinal.Value.ToShortDateString) Then ''''VALIDAMOS QUE LA FECHA INICIAL NO SEA MAYOR A LA FINAL
            MsgBox("La Fecha Inicial no puede ser Mayor a la Fecha Final", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.cmbCajera.Text.Length = 0 Then ''''VALIDAMOS QUE HAYA SELECCIONADO AL MENOS UN REGISTRO EN LOS CAJEROS
            MsgBox("Seleccione una opción en el Cajero(a)", MsgBoxStyle.Information)
            Exit Sub
        End If

        uspReporteTiposGastos(Me.cmbCajera.SelectedValue, Me.dtpFechaInicial.Value, Me.dtpFechaFinal.Value)
        Me.Close()
    End Sub
End Class