Imports System.Data.SqlClient

Public Class FormCAMDO
    Private opcionlocal As String = "N"

    Private Sub FormCAMDO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRACALLES)
        CON.Close()
        Me.Clv_ColoniaComboBox.Enabled = False
        Me.Clv_CiudadComboBox.Enabled = False

        opcionlocal = "N"
        'Me.CONCAMDOFACBindingSource.AddNew()
        Me.CONTRATOTextBox.Text = GloContrato
        Me.Clv_OrdenTextBox.Text = gloClv_Session
        'Me.ContratoTextBox1.Text = GloContrato
        'Me.Clv_SessionTextBox.Text = gloClv_Session
        Busca()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub CONCAMDOFACBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOFACBindingNavigatorSaveItem.Click

        If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Calle por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Colonia por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Or Len(Me.Clv_CiudadComboBox.Text) = 0 Then
            MsgBox("Seleccione la Ciudad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
            MsgBox("Capture las Entrecalles por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero Telef�nico por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        ''Me.CONCAMDOFACBindingSource.EndEdit()
        'Me.CONCAMDOFACTableAdapter.Connection = CON
        'Me.CONCAMDOFACTableAdapter.Update(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue))
        ''Me.ConCAMDOTMPTableAdapter.Connection = CON
        ''Me.ConCAMDOTMPTableAdapter.Update(gloClv_Session, GloContrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue))

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Sesion", SqlDbType.BigInt, gloClv_Session)
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@Clv_calle", SqlDbType.Int, Me.Clv_CalleComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Numero", SqlDbType.VarChar, NUMEROTextBox.Text, 50)
        BaseII.CreateMyParameter("@NumeroInt", SqlDbType.VarChar, NumIntTbox.Text, 50)
        BaseII.CreateMyParameter("@EntreCalles", SqlDbType.VarChar, Me.ENTRECALLESTextBox.Text, 250)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_ColoniaComboBox.SelectedValue)
        BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, Me.TELEFONOTextBox.Text, 50)
        BaseII.CreateMyParameter("@ClvTecnica", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_CiudadComboBox.SelectedValue)
        BaseII.Inserta("NUECAMDOFACnoInt")

        CON.Close()
        GloBndExt = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)

        'If IsNumeric(Me.CLAVETextBox.Text) = False Then
        '    MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
        '    Exit Sub
        'End If
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            MsgBox("No hay Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            MsgBox("No hay Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        CON.Open()
        Me.CONCAMDOFACTableAdapter.Delete(gloClv_Session, GloContrato)
        Me.CONCAMDOFACBindingSource.CancelEdit()
        'Me.ConCAMDOTMPTableAdapter.Connection = CON
        'Me.ConCAMDOTMPTableAdapter.Delete(gloClv_Session, GloContrato)
        CON.Close()

        GloBndExt = False
        Me.Close()
    End Sub

    Private Sub Clv_CiudadComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadComboBox.SelectedIndexChanged
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = True Then
            Me.Clv_CiudadComboBox.Enabled = True
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndExt = False
        Me.Close()
    End Sub

    Private Sub Clv_ColoniaComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaComboBox.SelectedIndexChanged
        Me.asignacolonia()
    End Sub

    Private Sub Clv_CalleComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleComboBox.SelectedIndexChanged
        asiganacalle()
    End Sub
    Private Sub Busca()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONCAMDOFACTableAdapter.Connection = CON
            Me.CONCAMDOFACTableAdapter.Fill(Me.NewsoftvDataSet2.CONCAMDOFAC, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(GloContrato, Long)))
            'Me.ConCAMDOTMPTableAdapter.Connection = CON
            'Me.ConCAMDOTMPTableAdapter.Fill(Me.DataSetEric3.ConCAMDOTMP, gloClv_Session, GloContrato)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub asignacolonia()
        Try
            If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewsoftvDataSet2.MuestraCVECOLCIU, Me.Clv_ColoniaComboBox.SelectedValue)
                CON.Close()
                If opcionlocal = "N" Then Me.Clv_CiudadComboBox.Text = ""
                Me.Clv_CiudadComboBox.Enabled = True

            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)


        End Try
    End Sub

    Private Sub asiganacalle()
        Try
            If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewsoftvDataSet2.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.Clv_CalleComboBox.SelectedValue, Integer)))
                CON.Close()
                Me.Clv_ColoniaComboBox.Enabled = True
                If opcionlocal = "N" Then Me.Clv_ColoniaComboBox.Text = ""
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class