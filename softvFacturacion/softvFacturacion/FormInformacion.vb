﻿Imports System
Imports System.Data.SqlClient

Public Class FormInformacion


    Public Sub SP_InformacionClienes(ByVal oContrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("SP_InformacionClienes", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = oContrato
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                Label1.Text = reader(0).ToString()
                Label3.Text = reader(1).ToString()
                Label4.Text = reader(2).ToString()
                Label5.Text = reader(3).ToString()
                Label6.Text = reader(4).ToString()
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub FormInformacion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' colorea(Me)
        SP_InformacionClienes(GloContrato)
    End Sub
End Class