Imports System.Data.SqlClient

Public Class FrmPagosAdelantados

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloBnd = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If Me.Panel1.Visible = True And (Me.NumericUpDown1.Value >= 10 And Me.NumericUpDown1.Value <= 12) And IdSistema = "SA" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Connection = CON
                Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Fill(Me.DataSetEdgar.ASIGNA_PrePorcentaje_Descuento, gloClv_Session, Me.NumericUpDown2.Value)
                CON.Close()
            End If



            If (Me.Panel1.Visible = True Or Me.Panel2.Visible = True) And (Me.NumericUpDown1.Value >= 6 And Me.NumericUpDown1.Value <= 12) And IdSistema = "AG" Then
                If Me.RadioButtonDescuento.Checked = True Then
                    Dim CON2 As New SqlConnection(MiConexion)
                    CON2.Open()
                    Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Connection = CON2
                    Me.ASIGNA_PrePorcentaje_DescuentoTableAdapter.Fill(Me.DataSetEdgar.ASIGNA_PrePorcentaje_Descuento, gloClv_Session, Me.NumericUpDown2.Value)
                    CON2.Close()
                ElseIf Me.RadioButtonMeses.Checked = True Then
                    AsigaMesesDeRegalo(gloClv_Session, Me.NumericUpDown3.Value)
                End If
            End If


            Glo_Apli_Pnt_Ade = False
            If Me.CheckBox1.CheckState = CheckState.Checked Then
                Glo_Apli_Pnt_Ade = True
            End If
            GloAdelantados = Me.NumericUpDown1.Value
            GloBnd = True
            Me.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub FrmPagosAdelantados_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub

    Private Sub FrmPagosAdelantados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.NumericUpDown2.Value = 0
        If IdSistema = "AG" Then
            Me.CheckBox1.Visible = False
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged

    End Sub

    Private Sub NumericUpDown1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NumericUpDown1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NumericUpDown1, Asc(LCase(e.KeyChar)), "N")))
    End Sub


    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
        Me.Panel3.Visible = False
        Me.NumericUpDown2.Value = 0
        Me.NumericUpDown3.Value = 0

        'If (Me.NumericUpDown1.Value >= 10 And Me.NumericUpDown1.Value <= 12) And IdSistema = "SA" Then
        '    Me.Panel1.Visible = True
        'ElseIf (Me.NumericUpDown1.Value >= 6 And Me.NumericUpDown1.Value <= 12) And IdSistema = "AG" Then
        '    Me.Panel1.Visible = True
        '    Me.Panel2.Visible = True
        '    Me.Panel3.Visible = True
        'End If
    End Sub


 



    Private Sub RadioButtonDescuento_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonDescuento.CheckedChanged
        If Me.RadioButtonDescuento.Checked = True Then
            Me.Panel1.Enabled = True
            Me.Panel2.Enabled = False
        End If
    End Sub

    Private Sub RadioButtonMeses_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonMeses.CheckedChanged
        If Me.RadioButtonMeses.Checked = True Then
            Me.Panel1.Enabled = False
            Me.Panel2.Enabled = True
        End If
    End Sub

    Private Sub AsigaMesesDeRegalo(ByVal Clv_Session As Long, ByVal Meses As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("AsigaMesesDeRegalo", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Meses", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = Meses
        comando.Parameters.Add(parametro1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
        End Try
    End Sub
End Class